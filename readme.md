# 服务中心

## 简述

服务中心,主要包含:

1. 常量定义,DB数据结构定义,runtime数据结构定义
2. BuildEngine(调度)PreBuildEngine(yml解析),RefreshEngine实现(刷新仓库),Engin管理
3. git操作(clone.checkout...)
4. 数据库升降级
5. 路由(业务,安装,鉴权)

## 流程图

![](https://static01.imgkr.com/temp/ef374de8b0d24b8e92f577cb87116d37.png)

## api接口文档

[api接口文档](./api.md)

## 开发

1. 环境准备

- `go1.16+`
- `Goland` or `VS Code`
- `Mysql5.7+`

2. 拉取代码

```
git clone https://gitee.com/gitee-go/server.git

cd server
```

3. go mod

```
go mod tidy
```

如果是私有仓库的话请使用,仓库根目录下的

```
./goget.sh  

 or  

./goget.bash
```

4. 启动server

VS Code `launch.json` 设置(参考):

```
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Launch Package",
            "type": "go",
            "request": "launch",
            "mode": "auto",
            "program": "${workspaceFolder}/main.go"
        }
    ]
}
```

Goland

![](https://static01.imgkr.com/temp/a1adbb84a46a4d95a3723a5e9384084a.png)

- tips: 启动成功后会在`用户目录下创建.giteego目录`

`.giteego/application.yaml`说明:

```yaml
server:
  host: http://localhost:8090               # server外部访问地址
  port: "8090"                              # server的端口
  login-key: jWgXS7tlfqSq4F8KnRVuuNM9XM7e   # runner与server通信的密钥
  workspace: ""
  limit:
    build: 10                               # 最大同时构建数
    parse: 10                               # 最大同时解析yml数
runner:
  port: "7030"                              # runner端口
  secret: jWgXS7tlfqSq4F8KnRVuuNM9XM7eP9iZ  # runner与server通信的密钥
  main-runner: true                         # 是否启动server的集成runner,一个server只能有一个main-runner
datasource:                                 # 数据库
  host: xx                      
  port: xx
  database: xx
  username: xx
  password: xx
  showsql: true
artifactory: {}                             #暂时没用
```

5. 编译启动

```
export CGO_ENABLED=0

go build -o server main.go

chmod +x server

./server
```

6. 访问`http://localhost:8090`

## 代码结构

comm :

```
.
├── app.go
├── db.go                   文件缓存操作包括set.clear等
├── hook
│   ├── action.go           webhook中的action常量
│   ├── git.go              结构体定义,包含Reference,Commit,User
│   ├── gitee
│   │   ├── gitee.go
│   │   └── hook_test.go
│   ├── pr.go              webhook的pull request结构体定义
│   ├── repo.go            webhook的代码仓库定义 
│   ├── webhook.go         webhook的接口和一些结构体定义  
│   └── zip_test.go
├── limit.go               同时解析yml数和构建数设置
├── migrate.go             数据库升降级工具
├── notice
│   └── notice.go          通知结构体和内容定义
├── plugin
│   └── plugin.go          存放于DB中的插件的相关操作,从db获取plugin,解析plugin等
├── repo
│   └── repo.go            仓库的相关操作,仓库目录打包zip
├── thirdapi.go            获取第三方的api client,包括gitee,github,gitlab等
├── uifls.go               ui的base64
├── workpath.go            包含工作路径的初始化和定义
├── yml                    流水线yml
│   ├── dag
│   │   └── dag.go         初始化dag,新增dag到db等操作
│   ├── disc.yaml
│   ├── vars
│   │   └── yml.go         流水线yml中的变量的相关操作,包含替换yml变量,替换流水线变量等
│   ├── veri.yaml
│   ├── yml.go             流水线yml的接口定义
│   └── yml_test.go
└── ymlconf.go             server的application.yaml的结构定义和相关操作,包含初始化,检查等

```

engine:

```
.
├── agent                 暂时没有用到
│   ├── client.go
│   └── clients.go
├── comm
│   ├── group.go          cmd命令组的结构体定义
│   ├── inter.go          PreBuild和Build引擎的接口定义
│   ├── job.go            构建过程中的job结构体定义
│   └── plugin.go         限制plugin
├── dao
│   └── update.go         runtime更新build.step状态的相关操作
├── local
│   ├── buildengine.go    Build引擎的实现
│   ├── buildtask.go      job调度的实现
│   ├── buildtaskc.go     job调度的效验 
│   ├── clientengine.go   管理runner,包含注册,发现,获取runner信息等
│   ├── prebuild_test.go
│   ├── prebuildengine.go PreBuild的实现
│   ├── refreshengine.go  调用api刷新代码仓库的引擎
│   └── taskbuild.go      PreBuild的task实现
├── mgr.go                引擎的管理,初始化,启动,停止等.
└── mgrs.go
```

route: 业务接口

```
.
├── api.go
├── apiManual.go
├── badge.go
├── hooks.go
├── login.go
├── namespace.go
├── notice.go
├── oauth.go
├── oauths.go
├── pipeline.go
├── plugin.go
├── pubs.go
├── repo.go
└── system.go
```

routehb:

```
.
├── auth.go             runner的请求鉴权
├── client.go           
└── runner.go           server与runner之间的通信,包括日志,制品等的传输,runner获取job等
```

routes: 安装的路由和实现

```
.
├── install.go
├── install1.go
└── install2.go
```

server:

```
.
├── command.go        server的cli实现
├── db.go             初始化数据库和文件缓存
├── route.go          路由的注册
├── server.go         服务启动
└── web.go
```

service:业务相关的实现

```
.
├── api.go
├── api_test.go
├── hooks.go
├── mid.go
├── notice.go
├── param.go
├── perms.go
├── token.go
└── user.go
```

7. 图

![](https://static01.imgkr.com/temp/b62a68df7e3d482790fce6d234dd7e72.png)

![](https://static01.imgkr.com/temp/65525495451b4b26959534560e2f32a0.png)

8. docker相关

使用docker-compose详情参见:  https://gitee.com/gitee-go/docker