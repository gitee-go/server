module gitee.com/gitee-go/server

go 1.15

require (
	gitee.com/gitee-go/core v0.0.0-20210122061720-337847920db1
	gitee.com/gitee-go/dag-core v0.0.0-20210608070036-1dae6eddb58c
	gitee.com/gitee-go/runner-core v0.0.0-20210107072439-f8647c7084f9
	gitee.com/gitee-go/utils v0.0.0-20210115102610-07f12f83acd2
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20210208195552-ff826a37aa15 // indirect
	github.com/boltdb/bolt v1.3.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-git/go-git/v5 v5.2.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/mgr9525/HyperByte-Transfer-Protocol v0.0.0-20210428085206-8dfaf346081f
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/sirupsen/logrus v1.7.0
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
	gopkg.in/yaml.v2 v2.4.0
)

replace (
	gitee.com/gitee-go/core => ../core
	gitee.com/gitee-go/dag-core => ../dag-core
	gitee.com/gitee-go/runner-core => ../runner-core
	gitee.com/gitee-go/utils => ../utils
)
