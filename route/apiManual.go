package route

import (
	"errors"
	"gitee.com/gitee-go/core"
	"gitee.com/gitee-go/core/common"
	"gitee.com/gitee-go/core/model/pipeline"
	"gitee.com/gitee-go/core/models"
	"gitee.com/gitee-go/server/comm"
	"gitee.com/gitee-go/server/engine"
	"gitee.com/gitee-go/server/service"
	"gitee.com/gitee-go/utils"
	"gitee.com/gitee-go/utils/ioex"
	"github.com/gin-gonic/gin"
)

type ManualController struct {
}

func (ManualController) GetPath() string {
	return "/api/manual"
}
func (c *ManualController) Routes(g gin.IRoutes) {
	g.Use(service.MidUserCheck)
	g.POST("/build", utils.GinReqParseJson(c.manualBuild))
	g.POST("/rebuild", utils.GinReqParseJson(c.manualRebuild))
}

// 手动执行api
func (ManualController) manualBuild(c *gin.Context, m *utils.Map) {
	pipelineId := m.GetString("pipelineId")
	branch := m.GetString("branch")
	if pipelineId == "" {
		core.NewUIRes(common.UICodeErrParam).
			SetMsgObjs(errors.New("请选择流水线")).
			ResJson(c)
		return
	}
	if branch == "" {
		core.NewUIRes(common.UICodeErrParam).
			SetMsgObjs("请选择分支").
			ResJson(c)
		return
	}
	db := comm.DBMain.GetDB()
	tp := &pipeline.TPipeline{}
	get, err := db.Where("id = ?", pipelineId).Get(tp)
	if err != nil {
		core.Log.Errorf("manualRun db err :%v", err)
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs("流水线异常，请重试").
			ResJson(c)
		return
	}
	if !get {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(errors.New("流水线不存在")).
			ResJson(c)
		return
	}
	if tp.RepoId == "" {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs("流水线未配置仓库！").
			ResJson(c)
		return
	}
	tr := &pipeline.TRepo{}
	get, err = db.Where("id = ?", tp.RepoId).Where("active != 0 and deleted != 1").Get(tr)
	if err != nil {
		core.Log.Errorf("manualRun db err :%v", err)
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs("仓库异常，请重试").
			ResJson(c)
		return
	}
	if !get {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs("流水线对应的仓库不存在！").
			ResJson(c)
		return
	}
	user := service.GetMidLgUser(c)
	err = engine.Mgr().GetPreBuildEgn().PutBuild(pipelineId, branch, user.Id)
	if err != nil {
		core.Log.Errorf("manualRun db err :%v", err)
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(err).
			ResJson(c)
		return
	}
	core.NewUIResOk().ResJson(c)
}

// 重新构建api
func (ManualController) manualRebuild(c *gin.Context, m *utils.Map) {
	pipelineVersionId := m.GetString("pipelineVersionId")
	if pipelineVersionId == "" {
		core.NewUIRes(common.UICodeErrParam).
			SetMsgObjs("请选择构建历史").
			ResJson(c)
		return
	}
	db := comm.DBMain.GetDB()
	tpv := &pipeline.TPipelineVersion{}
	get, err := db.Where("id = ?", pipelineVersionId).Get(tpv)
	if err != nil {
		core.Log.Errorf("manualRun db err :%v", err)
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(err).
			ResJson(c)
		return
	}
	if !get {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs("流水线不存在！").
			ResJson(c)
		return
	}
	if tpv.RepoId == "" {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs("流水线未配置仓库！").
			ResJson(c)
		return
	}
	tr := &pipeline.TRepo{}
	get, err = db.Where("id = ?", tpv.RepoId).Where("active !=0 and deleted != 1").Get(tr)
	if err != nil {
		core.Log.Errorf("manualRun db err :%v", err)
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(err).
			ResJson(c)
		return
	}
	if !get {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs("流水线对应的仓库不存在！").
			ResJson(c)
		return
	}
	user := service.GetMidLgUser(c)
	put, err := engine.Mgr().GetPreBuildEgn().PutRebuild(pipelineVersionId, user.Id)
	if err != nil {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(err).
			ResJson(c)
		return
	}
	mt := &models.TPipelineVersion{}
	err = ioex.StructToStructByMap(put, mt)
	if err != nil {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(err).
			ResJson(c)
		return
	}
	mt.Openid = tr.Openid
	core.NewUIResOk(mt).ResJson(c)

}
