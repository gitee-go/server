package route

import (
	"gitee.com/gitee-go/core"
	"gitee.com/gitee-go/core/bean/oauthBean"
	"gitee.com/gitee-go/core/bean/sysBean"
	"gitee.com/gitee-go/core/common"
	"gitee.com/gitee-go/server/service"
	"gitee.com/gitee-go/utils"
	"github.com/gin-gonic/gin"
)

type SystemController struct{}

func (SystemController) GetPath() string {
	return "/api/sys"
}
func (c *SystemController) Routes(g gin.IRoutes) {
	g.Use(service.MidUserCheck)
	g.POST("/set/sys", utils.GinReqParseJson(c.setSys))
	g.POST("/set/oauth/:nm", utils.GinReqParseJson(c.setOAuth))
}
func (SystemController) setSys(c *gin.Context, m *sysBean.ParamSys) {
	service.SetsParam(sysBean.ParamSysKey, m)
	core.NewUIResOk().ResJson(c)
}
func (SystemController) setOAuth(c *gin.Context, m *oauthBean.AppInfo) {
	nm := c.Param("nm")
	if nm == "" {
		core.NewUIRes(common.UICodeErrParam).
			ResJson(c)
		return
	}
	//lguser:=service.GetMidLgUser(c)
	par := &oauthBean.ParamOAuth{}
	service.GetsParam(oauthBean.ParamOAuthKey, par)
	if par.Source == nil {
		par.Source = make(map[string]*oauthBean.AppInfo)
	}
	par.Source[nm] = m
	err := service.SetsParam(oauthBean.ParamOAuthKey, par)
	if err != nil {
		core.NewUIRes(common.UICodeErrs).SetMsgObjs(err).
			ResJson(c)
		return
	}
	core.NewUIResOk().ResJson(c)
}
