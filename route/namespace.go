package route

import (
	"gitee.com/gitee-go/core"
	"gitee.com/gitee-go/core/common"
	"gitee.com/gitee-go/server/comm"
	"gitee.com/gitee-go/server/service"
	"gitee.com/gitee-go/utils"
	"github.com/gin-gonic/gin"
)

type NamespaceController struct {
}

func (NamespaceController) GetPath() string {
	return "/api/namespace"
}
func (c *NamespaceController) Routes(g gin.IRoutes) {
	g.Use(service.MidUserCheck)
	g.POST("/namespace", utils.GinReqParseJson(c.namespace))
}

func (NamespaceController) namespace(c *gin.Context) {
	user := service.GetMidLgUser(c)
	var args []interface{}
	// 驱虫查询
	sqls := "select distinct(namespace) from t_repo where t_repo.deleted != 1 "
	if !service.CheckUPermission(user, "admin") {
		sqls += "and id in (select repo_id from t_user_repo where user_id = ?) " // 加参数
		args = append(args, user.Id)
	}
	ls := make([]string, 0)
	err := comm.DBMain.GetDB().SQL(sqls, args...).Find(&ls)
	if err != nil {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(err).
			ResJson(c)
		return
	}
	core.NewUIResOk(ls).ResJson(c)
}
