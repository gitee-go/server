package route

import (
	"gitee.com/gitee-go/core"
	"gitee.com/gitee-go/core/common"
	"gitee.com/gitee-go/server/engine"
	"gitee.com/gitee-go/server/service"
	"github.com/gin-gonic/gin"
)

type ReposController struct {
}

func (ReposController) GetPath() string {
	return "/api/repos"
}
func (c *ReposController) Routes(g gin.IRoutes) {
	g.POST("/hooks/:hookType", c.hooks)
}

func (ReposController) hooks(c *gin.Context) { // 推送代码触发构建入口
	hookType := c.Param("hookType")
	if hookType == "" {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs("未知的hook类型").
			ResJson(c)
		return
	}
	parse, err := service.Parse(c.Request, hookType)
	if err == nil {
		err = engine.Mgr().GetPreBuildEgn().PutWebHook(parse)
	}
	if err != nil {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(err).
			ResJson(c)
		return
	}
	core.NewUIResOk().ResJson(c)
}
