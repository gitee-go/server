package route

import (
	"gitee.com/gitee-go/core"
	"gitee.com/gitee-go/core/common"
	"gitee.com/gitee-go/core/models"
	"gitee.com/gitee-go/server/comm"
	"gitee.com/gitee-go/server/engine"
	"gitee.com/gitee-go/server/service"
	"gitee.com/gitee-go/utils"
	"github.com/gin-gonic/gin"
)

type ApiController struct {
}

func (ApiController) GetPath() string {
	return "/api/comm"
}
func (c *ApiController) Routes(g gin.IRoutes) {
	g.Use(service.MidUserCheck)
	g.POST("/searchUser", utils.GinReqParseJson(c.searchUser))
	g.POST("/runners", utils.GinReqParseJson(c.runners))
}
func (ApiController) searchUser(c *gin.Context, m *utils.Map) {
	var rets []*models.SerchUser
	q := m.GetString("q")
	if q == "" {
		core.NewUIResOk(rets).ResJson(c)
		return
	}

	lks := "%" + q + "%"
	err := comm.DBMain.GetDB().
		Where("name like ? or nick like ?", lks, lks).
		Limit(5).Find(&rets)
	if err != nil {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(err).
			ResJson(c)
		return
	}

	core.NewUIResOk(rets).ResJson(c)
}
func (ApiController) runners(c *gin.Context, m *utils.Map) {
	ls := engine.Mgr().GetClientEgn().Runners()
	core.NewUIResOk(ls).ResJson(c)
}
