package route

import (
	"gitee.com/gitee-go/core"
	"gitee.com/gitee-go/core/common"
	"gitee.com/gitee-go/core/dbcore"
	"gitee.com/gitee-go/core/model/pipeline"
	"gitee.com/gitee-go/core/models"
	"gitee.com/gitee-go/server/comm"
	"gitee.com/gitee-go/server/service"
	"gitee.com/gitee-go/utils"
	"github.com/gin-gonic/gin"
	"time"
)

type NoticeController struct {
}

func (NoticeController) GetPath() string {
	return "/api/notice"
}
func (c *NoticeController) Routes(g gin.IRoutes) {
	g.Use(utils.MidAccessAllowFun, service.MidUserCheck)
	g.POST("/notices", utils.GinReqParseJson(c.notices))
	g.POST("/mark", utils.GinReqParseJson(c.mark))
	g.POST("/deleted", utils.GinReqParseJson(c.deleted))
	g.POST("/markAll", utils.GinReqParseJson(c.markAll))
}

func (NoticeController) notices(c *gin.Context, m *utils.Map) {
	pg, _ := m.GetInt("page")
	sz, _ := m.GetInt("size")
	tps := m.GetString("types")
	status := m.GetString("status")
	user := service.GetMidLgUser(c)
	ls := make([]*models.TMessage, 0) // 分页查询
	sqls := "SELECT {{select}}  FROM t_message tm LEFT JOIN t_user_msg tum ON  " +
		" tm.xid = tum.mid  WHERE tum.uid = ?  AND tum.deleted != 1"
	var args []interface{}
	args = append(args, user.Id)
	if status != "" {
		sqls += " AND tum.status = ? "
		args = append(args, status)
	}
	if tps != "" {
		sqls += " AND tm.types = ? "
		args = append(args, tps)
	}
	sqls += " order by tm.created desc"
	page, err := comm.DBMain.FindPages(&dbcore.PageGen{
		SQL:       sqls,
		Args:      args,
		CountCols: "tm.id",
		FindCols:  "tm.*,tum.status as r_status ,tum.created,tum.status,tum.readtm",
	}, &ls, pg, sz)
	if err != nil {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(err).
			ResJson(c)
		return
	}
	core.NewUIResOk(page).ResJson(c)
}

func (NoticeController) mark(c *gin.Context) {
	m := map[string][]interface{}{}
	err := c.Bind(&m)
	if err != nil {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(err).
			ResJson(c)
		return
	}
	var ids []interface{}
	var ok = false
	if ids, ok = m["msgIds"]; !ok {
		core.NewUIRes(common.UICodeErrParam).
			ResJson(c)
		return
	}
	if ids == nil || len(ids) <= 0 {
		core.NewUIRes(common.UICodeErrParam).
			ResJson(c)
		return
	}
	user := service.GetMidLgUser(c)
	db := comm.DBMain.GetDB()

	tum := &pipeline.TUserMsg{
		Status: 1,
		Readtm: time.Now(),
	}
	for _, mid := range ids {
		tm := &pipeline.TMessage{}
		get, err := db.Where("id = ?", mid).Get(tm)
		if !get {
			continue
		}
		if err != nil {
			core.Log.Errorf("mark msg db err:%v", err)
			continue
		}
		// 更新数据
		_, err = db.Cols("status,readtm").
			Where("uid = ? and deleted != 1 and mid = ?", user.Id, tm.Xid).
			Update(tum)
		if err != nil {
			core.Log.Errorf("mark msg db err:%v", err)
			continue
		}
	}
	core.NewUIResOk().ResJson(c)
}

func (NoticeController) markAll(c *gin.Context) {
	user := service.GetMidLgUser(c)
	db := comm.DBMain.GetDB()
	tum := &pipeline.TUserMsg{
		Status: 1,
		Readtm: time.Now(),
	}
	_, err := db.Cols("status,readtm").
		Where("uid = ? and deleted != 1 and status != 1 ", user.Id).
		Update(tum)
	if err != nil {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(err).
			ResJson(c)
		return
	}
	core.NewUIResOk().ResJson(c)
}

func (NoticeController) deleted(c *gin.Context) {
	m := map[string][]interface{}{}
	err := c.Bind(&m)
	if err != nil {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(err).
			ResJson(c)
		return
	}
	var ids []interface{}
	var ok = false
	if ids, ok = m["msgIds"]; !ok {
		core.NewUIRes(common.UICodeErrParam).
			ResJson(c)
		return
	}
	if ids == nil || len(ids) <= 0 {
		core.NewUIRes(common.UICodeErrParam).
			ResJson(c)
		return
	}
	user := service.GetMidLgUser(c)
	msg := &pipeline.TUserMsg{
		Deleted: 1,
	}
	db := comm.DBMain.GetDB() // 更新数据
	_, err = db.Cols("deleted").Where("uid = ? and deleted != 1", user.Id).In("id", ids).Update(msg)
	if err != nil {
		core.NewUIRes(common.UICodeErrs).
			SetMsgObjs(err).
			ResJson(c)
		return
	}
	core.NewUIResOk().ResJson(c)
}
