package routehb

import (
	"gitee.com/gitee-go/core/bean/hbtpBean"
	"gitee.com/gitee-go/server/engine"
	hbtp "github.com/mgr9525/HyperByte-Transfer-Protocol"
)

type ClientRPC struct{}

func (ClientRPC) AuthFun() hbtp.AuthFun {
	return servAuthToken
}

func (ClientRPC) Reg(c *hbtp.Context, m *hbtpBean.RegReq) {
	if m.Name == "" || m.Type == "" {
		c.ResString(hbtp.ResStatusErr, "param err")
		return
	}

	ids, err := engine.Mgr().GetClientEgn().Reg(c, m)
	if err != nil {
		c.ResString(hbtp.ResStatusErr, err.Error())
		return
	}

	c.ResString(hbtp.ResStatusOk, ids)
}
