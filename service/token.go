package service

import (
	"gitee.com/gitee-go/core"
	"gitee.com/gitee-go/core/model/pipeline"
	"gitee.com/gitee-go/server/comm"
)

// 查找用户的token
func FindUserToken(typ, oid string) (*pipeline.TUserToken, bool) {
	e := &pipeline.TUserToken{} //查用户的TOKEN
	ok, err := comm.DBMain.GetDB().Where("`type`=? and openid=?", typ, oid).Get(e)
	if err != nil {
		core.Log.Errorf("FindUserToken(%s:%s) err:%v", typ, oid, err)
	}
	return e, ok
}
