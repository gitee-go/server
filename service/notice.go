package service

import (
	"gitee.com/gitee-go/core"
	"gitee.com/gitee-go/core/model/pipeline"
	"gitee.com/gitee-go/server/comm"
	"gitee.com/gitee-go/server/comm/notice"
	"gitee.com/gitee-go/utils"
	"strconv"
	"time"
)

// 封装
func InsertNoticesByRepo(repo *pipeline.TRepo, n *notice.Notice) (rterr error) {
	return InsertNoticesByRepoId(repo.Id, n)
}

// 封装
func InsertNoticesByRepoOpenid(openid string, n *notice.Notice) (rterr error) {
	db := comm.DBMain.GetDB()
	repo := &pipeline.TRepo{}
	get, err := db.Where("openid =? ", openid).Where("deleted != 1").Get(repo)
	if err != nil {
		return err
	}
	if !get {
		return nil
	}
	return InsertNoticesByRepoId(repo.Id, n)
}

// 发送信息给某个仓库的所有人
func InsertNoticesByRepoId(id string, n *notice.Notice) (rterr error) {
	db := comm.DBMain.GetDB()
	turs := make([]*pipeline.TUserRepo, 0)
	err := db.Where("repo_id = ? ", id).Find(&turs)
	if err != nil {
		return err
	}
	message, err := InsertTMessage(n)
	if err != nil {
		return err
	}
	for _, tur := range turs {
		_, err = InsertTUserMsg(message, strconv.FormatInt(tur.UserId, 10))
		if err != nil {
			core.Log.Errorf("InsertTUserMsg db err: %v", err)
			continue
		}
	}
	return nil
}

// 发送信息给某个人
func InsertTUserMsg(m *pipeline.TMessage, sender string) (*pipeline.TUserMsg, error) {
	db := comm.DBMain.GetDB()
	tum := &pipeline.TUserMsg{
		Mid:     m.Xid,
		Uid:     sender,
		Created: time.Now(),
		Status:  0,
		Deleted: 0,
	}
	_, err := db.Insert(tum)
	if err != nil {
		return nil, err
	}
	return tum, nil
}

// 插入信息到数据库
func InsertTMessage(n *notice.Notice) (*pipeline.TMessage, error) {
	db := comm.DBMain.GetDB()
	msg := &pipeline.TMessage{
		Xid:     utils.NewXid(),
		Uid:     strconv.FormatInt(n.Receiver, 10),
		Title:   n.Title,
		Content: n.Content,
		Types:   n.Types,
		Created: time.Now(),
		Infos:   n.Infos,
		Url:     n.Url,
	}
	_, err := db.Insert(msg)
	if err != nil {
		return nil, err
	}
	return msg, nil
}
