package service

import (
	"encoding/json"
	"fmt"
	"gitee.com/gitee-go/core/models"
	"gopkg.in/yaml.v2"
	"os"
	"testing"
)

func TestYMLToFile(t *testing.T) {
	y := &models.Pipeline{}
	js := "{\n  \"version\": 1,\n  \"name\": \"go-test-pr\",\n  \"triggers\": {\n    \"push\": {\n      \"branches\": [\n        \"master\"\n      ]\n    }\n  },\n  \"stages\": [\n    {\n      \"stage\": null,\n      \"displayName\": \"第一个stage\",\n      \"name\": \"stage1\",\n      \"jobs\": [\n        {\n          \"job\": null,\n          \"displayName\": \"第一个job1\",\n          \"name\": \"job1\",\n          \"commands\": [\n            \"java -jar application.jar\"\n          ],\n          \"dependArtifacts\": [\n            {\n              \"type\": \"archive\",\n              \"repository\": \"java@1\",\n              \"name\": \"application\",\n              \"target\": \"./\",\n              \"isForce\": true\n            }\n          ]\n        }\n      ]\n    }\n  ]\n}"
	err := json.Unmarshal([]byte(js), y)
	if err != nil {
		fmt.Println(fmt.Errorf("json err : %v", err))
	}
	marshal, err := yaml.Marshal(y)
	if err != nil {
		fmt.Println(fmt.Errorf("yaml err : %v", err))
	}

	create, err := os.Create("giteego-abc.yaml")
	defer func() {
		errs := create.Close()
		if errs != nil {
			fmt.Println(fmt.Errorf("create Close err : %v", err))
		}
	}()

	if err != nil {
		fmt.Println(fmt.Errorf("create err : %v", err))
	}

	_, err = create.Write(marshal)
	if err != nil {
		fmt.Println(fmt.Errorf("WriteAt err : %v", err))
	}

	fmt.Println("end")

}
