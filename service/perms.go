package service

import (
	"gitee.com/gitee-go/core/model/pipeline"
	"github.com/gin-gonic/gin"
)

/**
无用
*/

func CheckPermission(uid int64, perms string) bool {
	usr, ok := GetUser(uid)
	if !ok {
		return false
	}
	return CheckUPermission(usr, perms)
}
func CheckUPermission(usr *pipeline.TUser, perms string) bool {
	if usr == nil {
		return false
	}
	if perms == "common" {
		return true
	} else if perms == "admin" && usr.Name == "admin" {
		return true
	}
	return false
}
func CheckCurrPermission(c *gin.Context, perms string) bool {
	usr, ok := CurrUserCache(c)
	if !ok {
		return false
	}
	return CheckUPermission(usr, perms)
}
