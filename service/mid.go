package service

import (
	"gitee.com/gitee-go/core"
	"gitee.com/gitee-go/core/common"
	"gitee.com/gitee-go/core/model/pipeline"
	"github.com/gin-gonic/gin"
)

const LgUserKey = "lguser"

// 用户身份获取中间件
func MidUserCheck(c *gin.Context) {
	usr, ok := CurrUserCache(c)
	if !ok {
		core.NewUIRes(common.UICodeAuth).ResJson(c)
		c.Abort()
	}
	c.Set(LgUserKey, usr)
	c.Next()
}

// 获取登录用户信息,加了上面的中间件,一般不会nil
func GetMidLgUser(c *gin.Context) *pipeline.TUser {
	usr, ok := c.Get(LgUserKey)
	if !ok {
		return nil
	}
	lguser, ok := usr.(*pipeline.TUser)
	if !ok {
		return nil
	}
	return lguser
}
