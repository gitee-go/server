-- ----------------------------
-- Table structure for t_artifact
-- ----------------------------
CREATE TABLE `t_artifact`
(
    `id`         varchar(64) NOT NULL,
    `job_id`     varchar(64) NULL DEFAULT NULL COMMENT 'jobid',
    `build_id`   varchar(64) NULL DEFAULT NULL,
    `stage_id`   varchar(64) NULL DEFAULT NULL,
    `build_name` varchar(100) NULL DEFAULT NULL COMMENT '构建状态',
    `stage_name` varchar(100) NULL DEFAULT NULL COMMENT '退出码',
    `job_name`   varchar(500) NULL DEFAULT NULL COMMENT '错误信息',
    `status`     varchar(100) NULL DEFAULT NULL COMMENT '名字',
    `created`    datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `updated`    datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `name`       varchar(255) NULL DEFAULT NULL COMMENT '版本',
    `scope`      varchar(255) NULL DEFAULT NULL,
    `path`       varchar(255) NULL DEFAULT NULL,
    `repository` varchar(255) NULL DEFAULT NULL,
    `value`      varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT = 'Artifact';

-- ----------------------------
-- Records of t_artifact
-- ----------------------------
-- ----------------------------
-- Table structure for t_artifact_archive
-- ----------------------------
CREATE TABLE `t_artifact_archive`
(
    `id`         bigint(20) NOT NULL AUTO_INCREMENT,
    `xid`        varchar(64) NOT NULL,
    `repo_id`    varchar(64) NULL DEFAULT NULL,
    `build_id`   varchar(64) NULL DEFAULT NULL,
    `stage_id`   varchar(64) NULL DEFAULT NULL,
    `job_id`     varchar(64) NULL DEFAULT NULL,
    `art_id`     varchar(64) NULL DEFAULT NULL,
    `repository` varchar(100) NULL DEFAULT NULL,
    `name`       varchar(200) NULL DEFAULT NULL,
    `created`    datetime(0) NULL DEFAULT NULL,
    PRIMARY KEY (`id`, `xid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- ----------------------------
-- Table structure for t_build
-- ----------------------------
CREATE TABLE `t_build`
(
    `id`                  varchar(64) NOT NULL,
    `pipeline_id`         varchar(64) NULL DEFAULT NULL,
    `pipeline_version_id` varchar(64) NULL DEFAULT NULL,
    `status`              varchar(100) NULL DEFAULT NULL COMMENT '构建状态',
    `error`               varchar(500) NULL DEFAULT NULL COMMENT '错误信息',
    `event`               varchar(100) NULL DEFAULT NULL COMMENT '事件',
    `time_stamp`          datetime(0) NULL DEFAULT NULL COMMENT '执行时长',
    `title`               varchar(255) NULL DEFAULT NULL COMMENT '标题',
    `message`             varchar(255) NULL DEFAULT NULL COMMENT '构建信息',
    `started`             datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
    `finished`            datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
    `created`             datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `updated`             datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `version`             varchar(255) NULL DEFAULT NULL COMMENT '版本',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT = '构建信息表';

-- ----------------------------
-- Records of t_build
-- ----------------------------
-- ----------------------------
-- Table structure for t_cmd_group
-- ----------------------------
CREATE TABLE `t_cmd_group`
(
    `id`       varchar(64) NOT NULL,
    `build_id` varchar(64) NULL DEFAULT NULL,
    `job_id`   varchar(64) NULL DEFAULT NULL,
    `name`     varchar(200) NULL DEFAULT NULL,
    `status`   varchar(50) NULL DEFAULT NULL,
    `num`      int(11) NULL DEFAULT NULL,
    `created`  datetime(0) NULL DEFAULT NULL,
    `started`  datetime(0) NULL DEFAULT NULL,
    `finished` datetime(0) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for t_cmd_line
-- ----------------------------
CREATE TABLE `t_cmd_line`
(
    `id`       varchar(64) NOT NULL,
    `group_id` varchar(64) NULL DEFAULT NULL,
    `build_id` varchar(64) NULL DEFAULT NULL,
    `job_id`   varchar(64) NULL DEFAULT NULL,
    `status`   varchar(50) NULL DEFAULT NULL,
    `num`      int(11) NULL DEFAULT NULL,
    `content`  text NULL,
    `created`  datetime(0) NULL DEFAULT NULL,
    `started`  datetime(0) NULL DEFAULT NULL,
    `finished` datetime(0) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- ----------------------------
-- Table structure for t_dag
-- ----------------------------
CREATE TABLE `t_dag`
(
    `id`          varchar(64) NOT NULL,
    `dag_content` longtext NULL,
    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_dag
-- ----------------------------
-- ----------------------------
-- Table structure for t_depend_artifact
-- ----------------------------
CREATE TABLE `t_depend_artifact`
(
    `id`           varchar(64) NOT NULL,
    `job_id`       varchar(64) NULL DEFAULT NULL COMMENT 'jobid',
    `build_id`     varchar(64) NULL DEFAULT NULL,
    `stage_id`     varchar(64) NULL DEFAULT NULL,
    `build_name`   varchar(100) NULL DEFAULT NULL COMMENT '构建状态',
    `stage_name`   varchar(100) NULL DEFAULT NULL COMMENT '退出码',
    `job_name`     varchar(500) NULL DEFAULT NULL COMMENT '错误信息',
    `status`       varchar(100) NULL DEFAULT NULL COMMENT '名字',
    `created`      datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `updated`      datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `name`         varchar(255) NULL DEFAULT NULL COMMENT '版本',
    `type`         varchar(255) NULL DEFAULT NULL,
    `is_force`     tinyint(1) NULL DEFAULT NULL,
    `repository`   varchar(255) NULL DEFAULT NULL,
    `target`       varchar(255) NULL DEFAULT NULL,
    `source_stage` varchar(255) NULL DEFAULT NULL,
    `source_job`   varchar(255) NULL DEFAULT NULL,
    `value`        varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT = 't_depend_artifact';

-- ----------------------------
-- Records of t_depend_artifact
-- ----------------------------
-- ----------------------------
-- Table structure for t_hook
-- ----------------------------
CREATE TABLE `t_hook`
(
    `id`        varchar(64) NOT NULL,
    `type`      varchar(255) NULL DEFAULT NULL,
    `snapshot`  longtext NULL,
    `status`    varchar(255) NULL DEFAULT NULL,
    `msg`       varchar(255) NULL DEFAULT NULL,
    `hook_type` varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_hook
-- ----------------------------
-- ----------------------------
-- Table structure for t_job
-- ----------------------------
CREATE TABLE `t_job`
(
    `id`                  varchar(64) NOT NULL,
    `build_id`            varchar(64) NULL DEFAULT NULL,
    `stage_id`            varchar(100) NULL DEFAULT NULL COMMENT '流水线id',
    `display_name`        varchar(255) NULL DEFAULT NULL,
    `pipeline_version_id` varchar(64) NULL DEFAULT NULL COMMENT '流水线id',
    `job`                 varchar(255) NULL DEFAULT NULL,
    `status`              varchar(100) NULL DEFAULT NULL COMMENT '构建状态',
    `exit_code`           bigint(20) NULL DEFAULT NULL COMMENT '退出码',
    `error`               varchar(500) NULL DEFAULT NULL COMMENT '错误信息',
    `name`                varchar(100) NULL DEFAULT NULL COMMENT '名字',
    `started`             datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
    `finished`            datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
    `created`             datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `updated`             datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `version`             varchar(255) NULL DEFAULT NULL COMMENT '版本',
    `errignore`           varchar(5) NULL DEFAULT NULL,
    `number`              bigint(20) NULL DEFAULT NULL,
    `commands`            text NULL,
    `depends_on`          json NULL,
    `image`               varchar(255) NULL DEFAULT NULL,
    `environments`        json NULL,
    `sort`                bigint(10) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT = 'job';

-- ----------------------------
-- Records of t_job
-- ----------------------------
-- ----------------------------
-- Table structure for t_message
-- ----------------------------
CREATE TABLE `t_message`
(
    `id`      bigint(20) NOT NULL AUTO_INCREMENT,
    `xid`     varchar(64) NOT NULL,
    `uid`     varchar(64) NULL DEFAULT NULL COMMENT '发送者（可空）',
    `title`   varchar(255) NULL DEFAULT NULL,
    `content` longtext NULL,
    `types`   varchar(50) NULL DEFAULT NULL,
    `created` datetime(0) NULL DEFAULT NULL,
    `infos`   text NULL,
    `url`     varchar(500) NULL DEFAULT NULL,
    PRIMARY KEY (`id`, `xid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_message
-- ----------------------------
-- ----------------------------
-- Table structure for t_param
-- ----------------------------
CREATE TABLE `t_param`
(
    `id`    bigint(20) NOT NULL AUTO_INCREMENT,
    `name`  varchar(100) NULL DEFAULT NULL,
    `title` varchar(255) NULL DEFAULT NULL,
    `data`  text NULL,
    `times` datetime(0) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_param
-- ----------------------------
-- ----------------------------
-- Table structure for t_permssion
-- ----------------------------
CREATE TABLE `t_permssion`
(
    `id`     bigint(20) NOT NULL AUTO_INCREMENT,
    `xid`    varchar(64) NOT NULL,
    `parent` varchar(64) NULL DEFAULT NULL,
    `title`  varchar(100) NULL DEFAULT NULL,
    `value`  varchar(100) NULL DEFAULT NULL,
    `times`  datetime(0) NULL DEFAULT CURRENT_TIMESTAMP (0),
    `sort`   int(11) NULL DEFAULT 10,
    PRIMARY KEY (`id`, `xid`) USING BTREE,
    INDEX    `IDX_sys_permssion_parent`(`parent`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_permssion
-- ----------------------------
INSERT INTO `t_permssion`
VALUES (1, 'common', NULL, '通用', 'common', '2019-10-23 11:29:32', 0);
INSERT INTO `t_permssion`
VALUES (2, 'login', 'common', '登录', 'login', '2019-10-23 11:29:40', 11);
INSERT INTO `t_permssion`
VALUES (3, 'uppass', 'common', '修改密码', 'comm:uppass', '2020-03-01 21:47:42', 12);
INSERT INTO `t_permssion`
VALUES (4, 'admin', NULL, '后台界面', 'admin', '2019-10-27 00:11:30', 2);
INSERT INTO `t_permssion`
VALUES (5, 'roles', NULL, '角色管理', 'role:list', '2019-11-03 13:48:58', 3);
INSERT INTO `t_permssion`
VALUES (6, 'role1', 'roles', '角色编辑', 'role:edit', '2019-11-03 13:49:21', 11);
INSERT INTO `t_permssion`
VALUES (7, 'role2', 'roles', '角色删除', 'role:del', '2019-11-03 13:49:22', 12);
INSERT INTO `t_permssion`
VALUES (8, 'grant', 'roles', '用户授权', 'user:grant', '2019-11-04 09:52:47', 13);
INSERT INTO `t_permssion`
VALUES (9, 'users', NULL, '用户管理', 'user:list', '2019-11-04 09:51:14', 4);
INSERT INTO `t_permssion`
VALUES (10, 'userxg', 'users', '用户编辑', 'user:edit', '2019-11-04 20:27:02', 10);

-- ----------------------------
-- Table structure for t_pipeline
-- ----------------------------
CREATE TABLE `t_pipeline`
(
    `id`            varchar(64) NOT NULL,
    `name`          varchar(255) NULL DEFAULT NULL,
    `repo_id`       varchar(64) NULL DEFAULT NULL,
    `display_name`  varchar(255) NULL DEFAULT NULL,
    `pipeline_type` varchar(255) NULL DEFAULT NULL,
    `json_content`  longtext NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT = '流水线';

-- ----------------------------
-- Records of t_pipeline
-- ----------------------------
-- ----------------------------
-- Table structure for t_pipeline_branch
-- ----------------------------
CREATE TABLE `t_pipeline_branch`
(
    `id`          varchar(64) NOT NULL,
    `repo_id`     varchar(64) NULL DEFAULT NULL,
    `pipeline_id` varchar(64) NULL DEFAULT NULL,
    `name`        varchar(100) NULL DEFAULT NULL,
    `deleted`     int(1) NULL DEFAULT 0,
    `create_time` datetime(0) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_pipeline_branch
-- ----------------------------
-- ----------------------------
-- Table structure for t_pipeline_version
-- ----------------------------
CREATE TABLE `t_pipeline_version`
(
    `id`                    varchar(64) NOT NULL,
    `number`                bigint(20) NULL DEFAULT NULL COMMENT '构建次数',
    `trigger`               varchar(100) NULL DEFAULT NULL COMMENT '触发方式',
    `events`                varchar(100) NULL DEFAULT NULL COMMENT '事件push、pr、note',
    `ref`                   varchar(255) NULL DEFAULT NULL,
    `branch`                varchar(255) NULL DEFAULT NULL,
    `repo_id`               varchar(64) NULL DEFAULT NULL,
    `repo_name`             varchar(255) NULL DEFAULT NULL,
    `commit_sha`            varchar(255) NULL DEFAULT NULL,
    `commit_message`        text NULL COMMENT '提交信息',
    `pipeline_name`         varchar(255) NULL DEFAULT NULL,
    `pipeline_display_name` varchar(255) NULL DEFAULT NULL,
    `pipeline_id`           varchar(64) NULL DEFAULT NULL,
    `version`               varchar(255) NULL DEFAULT NULL,
    `yml_content`           longtext NULL,
    `json_content`          longtext NULL,
    `create_time`           datetime(0) NULL DEFAULT NULL,
    `create_user`           varchar(255) NULL DEFAULT NULL,
    `create_user_id`        varchar(64) NULL DEFAULT NULL,
    `deleted`               tinyint(1) NULL DEFAULT 0,
    `target_repo_name`      varchar(255) NULL DEFAULT NULL,
    `target_repo_sha`       varchar(255) NULL DEFAULT NULL,
    `target_repo_ref`       varchar(255) NULL DEFAULT NULL,
    `target_repo_clone_url` varchar(255) NULL DEFAULT NULL,
    `status`                varchar(100) NULL DEFAULT NULL COMMENT '构建状态',
    `error`                 varchar(500) NULL DEFAULT NULL COMMENT '错误信息',
    `note`                  varchar(255) NULL DEFAULT NULL,
    `title`                 varchar(255) DEFAULT NULL,
    `pr_number`             bigint(20) DEFAULT NULL,
    `repo_clone_url`        varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_pipeline_version
-- ----------------------------
-- ----------------------------
-- Table structure for t_repo
-- ----------------------------
CREATE TABLE `t_repo`
(
    `id`           varchar(64) NOT NULL,
    `openid`       varchar(100) NULL DEFAULT NULL COMMENT '数据来源方唯一标识',
    `org`          varchar(100) NULL DEFAULT NULL,
    `owner`        varchar(255) NULL DEFAULT NULL,
    `name`         varchar(255) NULL DEFAULT NULL,
    `path`         varchar(255) NULL DEFAULT NULL,
    `namespace`    varchar(255) NULL DEFAULT NULL,
    `full_name`    varchar(255) NULL DEFAULT NULL,
    `url`          text NULL,
    `repo_type`    varchar(255) NULL DEFAULT NULL,
    `hook_secret`  varchar(255) NULL DEFAULT NULL,
    `active`       tinyint(1) NULL DEFAULT 0 COMMENT '0:未激活1:已激活',
    `deleted`      tinyint(1) NULL DEFAULT 0,
    `create_time`  datetime(0) NULL DEFAULT NULL,
    `update_time`  datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `refresh_time` datetime(0) NULL DEFAULT NULL,
    `hook_id`      varchar(255) NULL DEFAULT NULL,
    `visible`      tinyint(1) NULL DEFAULT NULL COMMENT '0:私有 1:内部公开 2:公开访问',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_repo
-- ----------------------------
-- ----------------------------
-- Table structure for t_repo_variable
-- ----------------------------
CREATE TABLE `t_repo_variable`
(
    `id`          varchar(64)  NOT NULL,
    `repo_id`     varchar(100) NOT NULL,
    `name`        varchar(255) DEFAULT NULL,
    `value`       longtext,
    `public`      int(1) DEFAULT '0' COMMENT '0:公开 1: 私密',
    `read_only`   int(1) DEFAULT '0' COMMENT '0:只读 1:可修改',
    `remarks`     varchar(255) DEFAULT NULL,
    `create_time` datetime     DEFAULT NULL,
    `update_time` datetime     DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



-- ----------------------------
-- Table structure for t_role
-- ----------------------------
CREATE TABLE `t_role`
(
    `id`    bigint(20) NOT NULL AUTO_INCREMENT,
    `xid`   varchar(64) NOT NULL,
    `title` varchar(100) NULL DEFAULT NULL,
    `perms` text NULL,
    `times` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP (0),
    PRIMARY KEY (`id`, `xid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role`
VALUES (1, 'common', '通用权限', 'common,login,uppass', '2019-10-23 11:31:34');
INSERT INTO `t_role`
VALUES (2, 'admin', '权限管理员', 'admin,roles,role1,role2,users,grant', '2019-11-03 13:49:35');

-- ----------------------------
-- Table structure for t_stage
-- ----------------------------
CREATE TABLE `t_stage`
(
    `id`                  varchar(64) NOT NULL,
    `pipeline_version_id` varchar(64) NULL DEFAULT NULL COMMENT '流水线id',
    `build_id`            varchar(64) NULL DEFAULT NULL,
    `status`              varchar(100) NULL DEFAULT NULL COMMENT '构建状态',
    `exit_code`           bigint(20) NULL DEFAULT NULL COMMENT '退出码',
    `error`               varchar(500) NULL DEFAULT NULL COMMENT '错误信息',
    `name`                varchar(255) NULL DEFAULT NULL COMMENT '名字',
    `display_name`        varchar(255) NULL DEFAULT NULL,
    `started`             datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
    `finished`            datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
    `created`             datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    `updated`             datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
    `version`             varchar(255) NULL DEFAULT NULL COMMENT '版本',
    `on_success`          varchar(5) NULL DEFAULT NULL,
    `on_failure`          varchar(5) NULL DEFAULT NULL,
    `sort`                bigint(10) NULL DEFAULT NULL,
    `stage`               varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT = 'stage';

-- ----------------------------
-- Records of t_stage
-- ----------------------------
-- ----------------------------
-- Table structure for t_sys_variable
-- ----------------------------
CREATE TABLE `t_sys_variable`
(
    `id`    varchar(64) NOT NULL,
    `name`  varchar(255) NULL DEFAULT NULL,
    `value` varchar(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_sys_variable
-- ----------------------------
-- ----------------------------
-- Table structure for t_user
-- ----------------------------
CREATE TABLE `t_user`
(
    `id`          bigint(20) NOT NULL AUTO_INCREMENT,
    `name`        varchar(100) NULL DEFAULT NULL,
    `pass`        varchar(255) NULL DEFAULT NULL,
    `nick`        varchar(100) NULL DEFAULT NULL,
    `avatar`      varchar(500) NULL DEFAULT NULL,
    `create_time` datetime(0) NULL DEFAULT NULL,
    `login_time`  datetime(0) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user`
VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '管理员', NULL, NOW(), NULL);
-- ----------------------------
-- Table structure for t_user_msg
-- ----------------------------
CREATE TABLE `t_user_msg`
(
    `id`           bigint(20) NOT NULL AUTO_INCREMENT,
    `mid`          varchar(64) NULL DEFAULT NULL,
    `uid`          varchar(64) NULL DEFAULT NULL COMMENT '收件人',
    `created`      datetime(0) NULL DEFAULT NULL,
    `readtm`       datetime(0) NULL DEFAULT NULL,
    `status`       int(11) NULL DEFAULT 0,
    `deleted`      int(1) NULL DEFAULT 0,
    `deleted_time` datetime(0) NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_user_msg
-- ----------------------------
-- ----------------------------
-- Table structure for t_user_repo
-- ----------------------------
CREATE TABLE `t_user_repo`
(
    `id`      varchar(64) NOT NULL,
    `repo_id` varchar(64) NOT NULL,
    `user_id` bigint(20) NOT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_user_repo
-- ----------------------------
-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
CREATE TABLE `t_user_role`
(
    `user_id`    bigint(20) NOT NULL,
    `role_codes` text NULL,
    `limits`     text NULL,
    PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_user_role
-- ----------------------------

-- ----------------------------
-- Table structure for t_user_token
-- ----------------------------
CREATE TABLE `t_user_token`
(
    `id`            bigint(20) NOT NULL AUTO_INCREMENT,
    `uid`           bigint(20) NULL DEFAULT NULL,
    `type`          varchar(50) NULL DEFAULT NULL,
    `openid`        varchar(100) NULL DEFAULT NULL,
    `name`          varchar(255) NULL DEFAULT NULL,
    `nick`          varchar(255) NULL DEFAULT NULL,
    `avatar`        varchar(500) NULL DEFAULT NULL,
    `access_token`  text NULL DEFAULT NULL,
    `refresh_token` text NULL DEFAULT NULL,
    `expires_in`    bigint(20) NULL DEFAULT 0,
    `expires_time`  datetime(0) NULL DEFAULT NULL,
    `refresh_time`  datetime(0) NULL DEFAULT NULL,
    `create_time`   datetime(0) NULL DEFAULT NULL,
    `tokens`        text NULL,
    `uinfos`        text NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_plugin
-- ----------------------------

-- ----------------------------
-- Table structure for t_plugin
-- ----------------------------
CREATE TABLE `t_plugin`
(
    `id`           varchar(64) NOT NULL,
    `types`        varchar(255) DEFAULT NULL,
    `name`         varchar(255) DEFAULT NULL,
    `description`  varchar(255) DEFAULT NULL,
    `display_name` varchar(255) DEFAULT NULL,
    `content`      longtext,
    `create_time`  datetime     DEFAULT NULL,
    `update_time`  datetime     DEFAULT NULL COMMENT '更新时间',
    `deleted`      int(1) DEFAULT NULL,
    `deleted_time` datetime     DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `t_plugin`(`id`, `types`, `name`, `description`, `display_name`, `content`, `create_time`, `update_time`,
                       `deleted`, `deleted_time`)
VALUES ('1', 'publish', 'maven', 'maven发布', 'maven发布',
        'displayName: maven 发布插件\ndescription: maven 发布插件\ntype: shell@bash\nparameter:\n  Durl:\n    type: string\n    displayName: maven中央仓库url\n    description: maven中央仓库url\n    required: true\n  DrepositoryId:\n    type: string\n    displayName: maven中央仓库url\n    description: maven中央仓库url\n    required: true\n  Dfile:\n    type: string\n    displayName: 制品\n    description: 制品\n    required: true\n  DpomFile:\n    type: string\n    displayName: pom.xml文件\n    description: pom.xml文件\n    required: true\n  settingPath:\n    type: string\n    displayName: setting文件\n    description: setting文件\n    required: true\n\n\nbeforeCommand:\n  - 初始化Java环境:\n    -  |\n      PLUGIN_PATH=\"$WORKPATH/plugins\"\n      plugName=\"jdk\"\n      plugVer=\"8u201\"\n      tarFileVer=\"jdk-$plugVer-linux-x64\"\n      tarFileVers=\"jdk1.8.0_201\"\n\n      java -version\n      if [ $? -ne 0 ];then\n        mkdir -p $PLUGIN_PATH\n        plugDirPth=$PLUGIN_PATH/$tarFileVers\n        if [ ! -d $plugDirPth ];then\n          tarFlpth=$PLUGIN_PATH/tools.tar.gz\n          echo \"start download $plugName-$plugVer\"\n          wget -O $tarFlpth -c https://mirrors.huaweicloud.com/java/jdk/8u201-b09/$tarFileVer.tar.gz\n          if [ $? -ne 0 ];then\n            rm -f $tarFlpth\n            echo \"wget download $plugName-$plugVer err\"\n            exit -1\n          fi\n\n          echo \"start tar tools.tar.gz\"\n          tar -xvf $tarFlpth -C $PLUGIN_PATH\n          if [ $? -ne 0 ];then\n            rm -f $plugDirPth\n            echo \"$plugName-$plugVer tar err\"\n            exit -2\n          fi\n          rm -f $tarFlpth\n        fi\n        export JAVA_HOME=$plugDirPth\n        export classpath=$JAVA_HOME/lib:$JAVA_HOME/lib/tools.jar\n        export PATH=$JAVA_HOME/bin:$JAVA_HOME/jre/bin:$PATH\n      fi\n  - 初始化Maven环境:\n    -  |\n      PLUGIN_PATH=\"$WORKPATH/plugins\"\n      plugName=\"apache-maven\"\n      plugVer=\"3.8.1\"\n      tarFileVer=\"apache-maven-$plugVer-bin\"\n      tarFileVers=\"apache-maven-$plugVer\"\n\n      mvn -v\n      if [ $? -ne 0 ];then\n        mkdir -p $PLUGIN_PATH\n        plugDirPth=$PLUGIN_PATH/$tarFileVers\n        if [ ! -d $plugDirPth ];then\n          tarFlpth=$PLUGIN_PATH/tools.tar.gz\n          echo \"start download $plugName-$plugVer\"\n          wget -O $tarFlpth -c https://repo.huaweicloud.com/apache/maven/maven-3/3.8.1/binaries/$tarFileVer.tar.gz\n          if [ $? -ne 0 ];then\n            rm -f $tarFlpth\n            echo \"wget download $plugName-$plugVer err\"\n            exit -1\n          fi\n\n          echo \"start tar tools.tar.gz\"\n          tar -xvf $tarFlpth -C $PLUGIN_PATH\n          if [ $? -ne 0 ];then\n            rm -f $plugDirPth\n            echo \"$plugName-$plugVer tar err\"\n            exit -2\n          fi\n          rm -f $tarFlpth\n        fi\n        export MAVEN_HOME=$plugDirPth\n        export PATH=$MAVEN_HOME/bin:$PATH\n      fi      \n  - 环境准备结束:\n    - mvn --version\n\nafterCommand:\n  - Deploy:\n    - mvn  clean install\n    - mvn deploy:deploy-file -Durl=${{Durl}} \\\n                       -DrepositoryId=${{DrepositoryId}} \\\n                       -Dfile=${{Dfile}} \\\n                       -DpomFile=${{DpomFile}} \\\n                       --settings ${{settingPath}}\n\n',
        '2021-07-08 14:40:04', NULL, 0, NULL);
INSERT INTO `t_plugin`(`id`, `types`, `name`, `description`, `display_name`, `content`, `create_time`, `update_time`,
                       `deleted`, `deleted_time`)
VALUES ('2', 'publish', 'npm', 'npm发布', 'npm发布',
        'displayName: npm 发布插件\ndescription: npm 发布插件\ntype: shell@bash\nparameter:\n  registry:\n    type: string\n    displayName: npm registry 文件\n    description: npm registry 文件\n    required: true\n\nbeforeCommand:\n  - 初始化Node环境:\n    -  |\n      PLUGIN_PATH=\"$WORKPATH/plugins\"\n      plugName=\"nodejs\"\n      plugVer=\"14.17.0\"\n      tarFileVer=\"v$plugVer\"\n      tarFileVers=\"node-$tarFileVer-linux-x64\"\n\n      node -v\n      if [ $? -ne 0 ];then\n        mkdir -p $PLUGIN_PATH\n        plugDirPth=$PLUGIN_PATH/$tarFileVers\n        if [ ! -d $plugDirPth ];then\n          tarFlpth=$PLUGIN_PATH/tools.tar.gz\n          echo \"start download $plugName-$plugVer\"\n          wget -O $tarFlpth -c http://180.76.38.108:7039/node-$tarFileVer-linux-x64.tar.xz\n          if [ $? -ne 0 ];then\n            rm -f $tarFlpth\n            echo \"wget download $plugName-$plugVer err\"\n            exit -1\n          fi\n\n          echo \"start tar tools.tar.gz\"\n          tar -xvf $tarFlpth -C $PLUGIN_PATH\n          if [ $? -ne 0 ];then\n            rm -f $plugDirPth\n            echo \"$plugName-$plugVer tar err\"\n            exit -2\n          fi\n          rm -f $tarFlpth\n        fi\n        export NODE_HOME=$plugDirPth\n        export PATH=$NODE_HOME/bin:$PATH\n      fi\n  - 环境准备结束:\n    - node -v\n\nafterCommand:\n  - Publish:\n    - npm publish --registry=${{registry}}\n\n\n\n\n',
        '2021-07-08 16:49:08', '2021-07-08 16:49:10', 0, NULL);
INSERT INTO `t_plugin`(`id`, `types`, `name`, `description`, `display_name`, `content`, `create_time`, `update_time`,
                       `deleted`, `deleted_time`)
VALUES ('5', 'publish', 'composer', 'Composer', 'Composer',
        'displayName: packagist 发布插件\ndescription: packagist 发布插件\ntype: shell@bash\nparameter:\n  UserName:\n    type: string\n    displayName: 用户名\n    description: 用户名\n    required: true\n  ApiToken:\n    type: string\n    displayName: apiToken\n    description: apiToken\n    required: true\n  PackagistPackageUrl:\n    type: string\n    displayName: PackagistPackageUrl\n    description: PackagistPackageUrl\n    required: true\n\nbeforeCommand:\n- Deploy:\n    - curl -XPOST -H\'content-type:application/json\' \'https://packagist.org/api/update-package?username=${{UserName}}&apiToken=${{ApiToken}}\' -d\'{\"repository\":{\"url\":\"${{PackagistPackageUrl}}\"}}\'', NULL, NULL, 0, NULL);

INSERT INTO `t_plugin`(`id`, `types`, `name`, `description`, `display_name`, `content`, `create_time`, `update_time`, `deleted`, `deleted_time`) VALUES ('6', 'deploy', 'ssh', 'sshpass', 'sshpass', 'displayName: sshpass\ndescription: sshpass\ntype: shell@bash\nparameter:\n  Host:\n    type: string\n    displayName:   Host\n    description:   Host\n    required: true\n  UserName:\n    type: string\n    displayName:   UserName\n    description:   UserName\n    required: true\n  Pass:\n    type: string\n    displayName:   密码\n    description:   密码\n    required: true\n  FileName:\n    type: string\n    displayName:   文件名\n    description:   文件名\n    required: true\n  commands:         \n    type: shell     \n    rule: [ \"sh\", \"bash\" ]\n    displayName: 自定义构建脚本\n    description: 自定义构建脚本\n    required: true\n\nbeforeCommand:\n  - 上传:\n    - sshpass -p ${{Pass}} scp ${{FileName}} ${{UserName}}@${{Host}}:${{FileName}}\n\n', NULL, NULL, 0, NULL);
-- ----------------------------
-- Records of t_user_token
-- ----------------------------
-- ----------------------------
-- Table structure for t_webhooks
-- ----------------------------
/*CREATE TABLE `t_webhooks`  (
                               `id` varchar(64) NOT NULL,
                               `name` varchar(255) NULL DEFAULT NULL,
                               PRIMARY KEY (`id`) USING BTREE
);

-- ----------------------------
-- Records of t_webhooks
-- ----------------------------
-- ----------------------------
-- Table structure for t_yml_verification
-- ----------------------------
CREATE TABLE `t_yml_verification`  (
                                       `id` varchar(64) NOT NULL,
                                       `version` varchar(255) NULL DEFAULT NULL,
                                       PRIMARY KEY (`id`) USING BTREE
);

-- ----------------------------
-- Records of t_yml_verification
-- ----------------------------

-- ----------------------------
-- Table structure for t_yml_version
-- ----------------------------
CREATE TABLE `t_yml_version`  (
                                  `id` varchar(64) NOT NULL,
                                  `version` varchar(255) NULL DEFAULT NULL,
                                  PRIMARY KEY (`id`) USING BTREE
);
*/
-- ----------------------------
-- Records of t_yml_version
-- ----------------------------

