package server

import (
	"fmt"
	"gitee.com/gitee-go/core"
	"gitee.com/gitee-go/server/comm"
	"gitee.com/gitee-go/server/engine"
	"gitee.com/gitee-go/server/routes"
	"gitee.com/gitee-go/utils"
	"github.com/gin-gonic/gin"
	"os"
	"time"
)

func runWeb() {
	if comm.Port <= 0 {
		comm.Port = 8080
	}
	web := gin.Default()
	web.Use(utils.MidAccessAllowFun)
	web.Use(midUiHandle)
	comm.Web = web
	//regApi()
	if err := web.Run(fmt.Sprintf(":%d", comm.Port)); err != nil {
		core.Log.Debugf("web Run err: %v", err.Error())
		engine.Stop()
		os.Exit(-1)
	}
}
func installFun() {
	time.Sleep(time.Second)
	comm.Installed = false
	utils.GinRegController(comm.Web, &routes.InstallController{})
	for !comm.Installed {
		time.Sleep(time.Millisecond * 100)
	}
}
