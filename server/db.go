package server

import (
	"fmt"
	"gitee.com/gitee-go/core"
	"gitee.com/gitee-go/core/dbcore"
	"gitee.com/gitee-go/server/comm"
	"github.com/boltdb/bolt"
	"path/filepath"
)

/*
InitDB :init db
*/
func initDB(cf *comm.DataSourceConf) error {
	if comm.DBMain != nil {
		return nil
	}
	ul := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true",
		cf.Username,
		cf.Password,
		cf.Host,
		cf.Port,
		cf.Database)
	mysqlDB, err := dbcore.InitMysql(ul, cf.ShowSQL)
	if err != nil {
		core.Log.Errorf("InitMysql err:%v", err)
		return err
	}
	comm.DBMain = dbcore.NewDBHelper(mysqlDB)
	return nil
}

func initCache() error {
	db, err := bolt.Open(filepath.Join(comm.WorkPath, "cache.db"), 0640, nil)
	if err != nil {
		core.Log.Errorf("InitCache err:%v", err)
		return err
	}
	comm.BCache = db
	return nil
}
