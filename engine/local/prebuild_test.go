package local

import (
	"context"
	"fmt"
	"gitee.com/gitee-go/core"
	"gitee.com/gitee-go/utils/gitex"
	"github.com/go-git/go-git/v5"
	ghttp "github.com/go-git/go-git/v5/plumbing/transport/http"
	"runtime"
	"strings"
	"testing"
	"time"
)

type taskPreBuildTest struct {
	ctx    context.Context
	id     string
	end    bool
	cancal context.CancelFunc
}

func Test1(t *testing.T) {
	StartPreBuildEngineTest(context.Background())
}

func StartPreBuildEngineTest(ctx context.Context) {
	timeout, cancelFunc := context.WithTimeout(ctx, time.Second*2)
	c := &taskPreBuildTest{
		ctx:    timeout,
		cancal: cancelFunc,
	}
	go Job(c)
	cancelFunc()
	cancelFunc()
	for {
		time.Sleep(time.Second * 1)
		fmt.Println(c.ctx.Err())
		fmt.Println(runtime.NumGoroutine())
	}
}

func Job(c *taskPreBuildTest) {
	defer func() {
		c.cancal()
		c.end = true
		fmt.Println(c.end)
	}()
	fmt.Println("job start ")
	time.Sleep(time.Second * 4)
	fmt.Println("job end ")
}

func TestName1(t *testing.T) {
	//auth := &ghttp.TokenAuth{Token: "2f23412a33c0f7c9bca2cc80055403fd721578456c5c769070ae108d39c9676e"}
	basicAuth := &ghttp.BasicAuth{}
	basicAuth.Username = "oauth2"
	basicAuth.Password = "2f23412a33c0f7c9bca2cc80055403fd721578456c5c769070ae108d39c9676e"
	//gc := &git.CloneOptions{
	//	URL:           "https://oauth2:2f23412a33c0f7c9bca2cc80055403fd721578456c5c769070ae108d39c9676e@gitlab.com/jimbirthday/123321.git",
	//	ReferenceName: plumbing.ReferenceName("refs/heads/main"),
	//}
	gc := &git.CloneOptions{
		URL:  "https://gitlab.com/jimbirthday/123321.git",
		Auth: basicAuth,
	}
	repository, err := gitex.CloneRepo("./a", gc, context.Background())
	if err != nil {
		core.Log.Errorf("git CloneRepo2 err: %v", err)
		return
	}
	fmt.Println(repository)

}

func Test123(t *testing.T) {
	s := "@34ff12"
	fmt.Println("len", len(strings.Split(s, "@")))

}
