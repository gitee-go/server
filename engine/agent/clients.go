package agent

import "gitee.com/gitee-go/core/runtime"

type mpCliFun func(prt *Client, msg *runtime.ClientMsg)

var mpCliFn = map[string]mpCliFun{
	"heart": onHeart,
}

func onHeart(prt *Client, msg *runtime.ClientMsg) {
	prt.Send("heart", nil)
}
