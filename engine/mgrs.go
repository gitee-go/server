package engine

import (
	"gitee.com/gitee-go/server/comm"
	commes "gitee.com/gitee-go/server/engine/comm"
	"io/ioutil"
	"path/filepath"
)

func (c *Manager) initPlugs() {
	//commes.PushYmlPlug(yamlGolang)
	//commes.PushYmlPlug(yamlJava)
	//commes.PushYmlPlug(yamlMaven)
	//commes.PushYmlPlug(yamlGradle)
	//commes.PushYmlPlug(yamlNode)
	//commes.PushYmlPlug(yamlTest)
	// 查询工作目录下plugs目录的所有插件并解析到内存,提供流水线查找引用
	dir := filepath.Join(comm.WorkPath, comm.Plugs)
	fls, err := ioutil.ReadDir(dir)
	if err != nil {
		return
	}
	for _, v := range fls {
		ext := filepath.Ext(v.Name())
		if ext == ".yml" || ext == ".yaml" {
			bts, err := ioutil.ReadFile(filepath.Join(dir, v.Name()))
			if err == nil {
				commes.PushYmlPlug(bts)
			}
		}
	}
}
