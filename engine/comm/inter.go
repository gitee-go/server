package comm

import (
	"errors"
	"gitee.com/gitee-go/core/model/pipeline"
	"gitee.com/gitee-go/core/runtime"
)

/**
本身是不需要做接口抽象化处理的
但是考虑到以后有可能做横向扩展,需要实现分布式调度问题,所以抽象一个实现
*/

var BuildErrNoJob = errors.New("job not found")

type IBuildEngine interface {
	Put(cfg *runtime.Build) error
	PullJob(string) *SyncJob
	JobUpdate(req *runtime.ReqJobUpdate) error
	StopTask(bdid string) bool
	GetTaskStat(bdid string) (*runtime.BuildTaskStat, bool)
	GetSyncJob(bdid, snm, jnm string) (*SyncJob, bool)
	FindSyncJob(bdid, sid, jnm string) (*SyncJob, bool)
}

type IPreBuildEngine interface {
	PutWebHook(ptr *runtime.PreBuild) error
	PutRebuild(pipelineVersionId string, userId int64) (t *pipeline.TPipelineVersion, rterr error)
	PutBuild(pipelineId, branch string, userId int64) (err error)
}
