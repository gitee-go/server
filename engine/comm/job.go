package comm

import (
	"gitee.com/gitee-go/core/runtime"
	"gitee.com/gitee-go/runner-core/comm"
	"sync"
)

/**
server和runner在获取和修改job状态的时候,有可能冲突
所以封装此结构做并发处理
*/
type SyncJob struct {
	lk  sync.Mutex
	End bool
	et  *runtime.Job
	job *comm.RunJob
}

func NewSyncJob(job *runtime.Job) *SyncJob {
	return &SyncJob{
		et: job,
		job: &comm.RunJob{
			Id:              job.Id,
			StageId:         job.StageId,
			BuildId:         job.BuildId,
			Job:             job.Job,
			Name:            job.Name,
			Env:             job.Environments,
			Artifacts:       job.Artifacts,
			DependArtifacts: job.DependArtifacts,
			DependsOn:       job.DependsOn,
		},
	}
}
func (c *SyncJob) Lock() {
	c.lk.Lock()
}
func (c *SyncJob) Unlock() {
	c.lk.Unlock()
}
func (c *SyncJob) Job() *runtime.Job {
	return c.et
}
func (c *SyncJob) Runjb() *comm.RunJob {
	return c.job
}
