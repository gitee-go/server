package comm

import (
	"gitee.com/gitee-go/core/model/pipeline"
	"time"
)

type CmdGroupJson struct {
	Id      string
	BuildId string
	JobId   string
	Name    string
	Status  string
	Error   string
	Times   time.Time
	Cmds    []*pipeline.TCmdLine
}
