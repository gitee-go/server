package comm

import (
	"gitee.com/gitee-go/core/common"
	"gitee.com/gitee-go/core/thirdapi"
	"gitee.com/gitee-go/core/thirdapi/giteeapi"
)

var (
	apiClient *thirdapi.Client
)

func GetThirdApi(s string, host string) (*thirdapi.Client, error) {
	if apiClient == nil {
		switch s {
		case common.SourceGitee:
			apiClient = giteeapi.NewDefault()
		default:
			apiClient = giteeapi.NewDefault()
		}
	}
	return apiClient, nil
}
