package comm

import (
	"gitee.com/gitee-go/utils"
	"os"
	"path/filepath"
)

const (
	// PathPrefix : / or \\
	PathPrefix = string(filepath.Separator)
	// YamlDir : .giteego
	YamlDir = ".giteego"
	// Repo : repo
	Repo = "repo"
	// Build : build
	Build = "build"
	// Jobs : jobs
	Jobs = "jobs"
	// Plugs : plugins
	Plugs = "plugins"
)

/*
WorkPath is :server workpath
*/
var WorkPath = ""

/*
InitWorkPath init WorkPath
*/
func initWorkPath() {
	if MainCfg.ServerConf.WorkPath != "" {
		WorkPath = MainCfg.ServerConf.WorkPath
	}
	if WorkPath == "" {
		WorkPath = filepath.Join(utils.HomePath(), ".giteeGo", "server")
	}
	os.MkdirAll(WorkPath, 0755)
}
