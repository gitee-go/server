package gitee

import (
	"encoding/json"
	"fmt"
	"gitee.com/gitee-go/core"
	"gitee.com/gitee-go/core/common"
	"gitee.com/gitee-go/server/comm/hook"
	"io/ioutil"
	"net/http"
	"strconv"
	"testing"
	"time"
)

func TestName(t *testing.T) {
	client := &http.Client{
		Timeout: time.Second * 8,
	}
	request, err := http.NewRequest("GET", "https://api.github.com/repos/jimbirthday/golang-tcp/pulls/1", nil)
	if err != nil {
		core.Log.Errorf("github convertCommentHook CommentsUrl err %v", err)
		return
	}
	res, err := client.Do(request)
	if err != nil {
		core.Log.Errorf("github convertCommentHook Do err %v", err)
		return
	}
	all, err := ioutil.ReadAll(res.Body)
	if err != nil {
		core.Log.Errorf("github convertCommentHook ReadAll err %v", err)
		return
	}
	defer res.Body.Close()
	fmt.Println(string((all)))
}

func TestName2(t *testing.T) {
	str := []byte(`{
  "url": "https://api.github.com/repos/jimbirthday/golang-tcp/pulls/1",
  "id": 671072923,
  "node_id": "MDExOlB1bGxSZXF1ZXN0NjcxMDcyOTIz",
  "html_url": "https://github.com/jimbirthday/golang-tcp/pull/1",
  "diff_url": "https://github.com/jimbirthday/golang-tcp/pull/1.diff",
  "patch_url": "https://github.com/jimbirthday/golang-tcp/pull/1.patch",
  "issue_url": "https://api.github.com/repos/jimbirthday/golang-tcp/issues/1",
  "number": 1,
  "state": "open",
  "locked": false,
  "title": "Update a.yaml",
  "user": {
    "login": "jimbirthday",
    "id": 38313933,
    "node_id": "MDQ6VXNlcjM4MzEzOTMz",
    "avatar_url": "https://avatars.githubusercontent.com/u/38313933?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/jimbirthday",
    "html_url": "https://github.com/jimbirthday",
    "followers_url": "https://api.github.com/users/jimbirthday/followers",
    "following_url": "https://api.github.com/users/jimbirthday/following{/other_user}",
    "gists_url": "https://api.github.com/users/jimbirthday/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/jimbirthday/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/jimbirthday/subscriptions",
    "organizations_url": "https://api.github.com/users/jimbirthday/orgs",
    "repos_url": "https://api.github.com/users/jimbirthday/repos",
    "events_url": "https://api.github.com/users/jimbirthday/events{/privacy}",
    "received_events_url": "https://api.github.com/users/jimbirthday/received_events",
    "type": "User",
    "site_admin": false
  },
  "body": "123",
  "created_at": "2021-06-16T06:46:35Z",
  "updated_at": "2021-06-16T07:21:39Z",
  "closed_at": null,
  "merged_at": null,
  "merge_commit_sha": "3777f4800a025cb762218277329868eb10f37496",
  "assignee": null,
  "assignees": [

  ],
  "requested_reviewers": [

  ],
  "requested_teams": [

  ],
  "labels": [

  ],
  "milestone": null,
  "draft": false,
  "commits_url": "https://api.github.com/repos/jimbirthday/golang-tcp/pulls/1/commits",
  "review_comments_url": "https://api.github.com/repos/jimbirthday/golang-tcp/pulls/1/comments",
  "review_comment_url": "https://api.github.com/repos/jimbirthday/golang-tcp/pulls/comments{/number}",
  "comments_url": "https://api.github.com/repos/jimbirthday/golang-tcp/issues/1/comments",
  "statuses_url": "https://api.github.com/repos/jimbirthday/golang-tcp/statuses/9a0fc20aace682c1e7dc54865e7a5eca7870d027",
  "head": {
    "label": "jimbirthday:dev",
    "ref": "dev",
    "sha": "9a0fc20aace682c1e7dc54865e7a5eca7870d027",
    "user": {
      "login": "jimbirthday",
      "id": 38313933,
      "node_id": "MDQ6VXNlcjM4MzEzOTMz",
      "avatar_url": "https://avatars.githubusercontent.com/u/38313933?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/jimbirthday",
      "html_url": "https://github.com/jimbirthday",
      "followers_url": "https://api.github.com/users/jimbirthday/followers",
      "following_url": "https://api.github.com/users/jimbirthday/following{/other_user}",
      "gists_url": "https://api.github.com/users/jimbirthday/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/jimbirthday/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/jimbirthday/subscriptions",
      "organizations_url": "https://api.github.com/users/jimbirthday/orgs",
      "repos_url": "https://api.github.com/users/jimbirthday/repos",
      "events_url": "https://api.github.com/users/jimbirthday/events{/privacy}",
      "received_events_url": "https://api.github.com/users/jimbirthday/received_events",
      "type": "User",
      "site_admin": false
    },
    "repo": {
      "id": 375623429,
      "node_id": "MDEwOlJlcG9zaXRvcnkzNzU2MjM0Mjk=",
      "name": "golang-tcp",
      "full_name": "jimbirthday/golang-tcp",
      "private": false,
      "owner": {
        "login": "jimbirthday",
        "id": 38313933,
        "node_id": "MDQ6VXNlcjM4MzEzOTMz",
        "avatar_url": "https://avatars.githubusercontent.com/u/38313933?v=4",
        "gravatar_id": "",
        "url": "https://api.github.com/users/jimbirthday",
        "html_url": "https://github.com/jimbirthday",
        "followers_url": "https://api.github.com/users/jimbirthday/followers",
        "following_url": "https://api.github.com/users/jimbirthday/following{/other_user}",
        "gists_url": "https://api.github.com/users/jimbirthday/gists{/gist_id}",
        "starred_url": "https://api.github.com/users/jimbirthday/starred{/owner}{/repo}",
        "subscriptions_url": "https://api.github.com/users/jimbirthday/subscriptions",
        "organizations_url": "https://api.github.com/users/jimbirthday/orgs",
        "repos_url": "https://api.github.com/users/jimbirthday/repos",
        "events_url": "https://api.github.com/users/jimbirthday/events{/privacy}",
        "received_events_url": "https://api.github.com/users/jimbirthday/received_events",
        "type": "User",
        "site_admin": false
      },
      "html_url": "https://github.com/jimbirthday/golang-tcp",
      "description": null,
      "fork": false,
      "url": "https://api.github.com/repos/jimbirthday/golang-tcp",
      "forks_url": "https://api.github.com/repos/jimbirthday/golang-tcp/forks",
      "keys_url": "https://api.github.com/repos/jimbirthday/golang-tcp/keys{/key_id}",
      "collaborators_url": "https://api.github.com/repos/jimbirthday/golang-tcp/collaborators{/collaborator}",
      "teams_url": "https://api.github.com/repos/jimbirthday/golang-tcp/teams",
      "hooks_url": "https://api.github.com/repos/jimbirthday/golang-tcp/hooks",
      "issue_events_url": "https://api.github.com/repos/jimbirthday/golang-tcp/issues/events{/number}",
      "events_url": "https://api.github.com/repos/jimbirthday/golang-tcp/events",
      "assignees_url": "https://api.github.com/repos/jimbirthday/golang-tcp/assignees{/user}",
      "branches_url": "https://api.github.com/repos/jimbirthday/golang-tcp/branches{/branch}",
      "tags_url": "https://api.github.com/repos/jimbirthday/golang-tcp/tags",
      "blobs_url": "https://api.github.com/repos/jimbirthday/golang-tcp/git/blobs{/sha}",
      "git_tags_url": "https://api.github.com/repos/jimbirthday/golang-tcp/git/tags{/sha}",
      "git_refs_url": "https://api.github.com/repos/jimbirthday/golang-tcp/git/refs{/sha}",
      "trees_url": "https://api.github.com/repos/jimbirthday/golang-tcp/git/trees{/sha}",
      "statuses_url": "https://api.github.com/repos/jimbirthday/golang-tcp/statuses/{sha}",
      "languages_url": "https://api.github.com/repos/jimbirthday/golang-tcp/languages",
      "stargazers_url": "https://api.github.com/repos/jimbirthday/golang-tcp/stargazers",
      "contributors_url": "https://api.github.com/repos/jimbirthday/golang-tcp/contributors",
      "subscribers_url": "https://api.github.com/repos/jimbirthday/golang-tcp/subscribers",
      "subscription_url": "https://api.github.com/repos/jimbirthday/golang-tcp/subscription",
      "commits_url": "https://api.github.com/repos/jimbirthday/golang-tcp/commits{/sha}",
      "git_commits_url": "https://api.github.com/repos/jimbirthday/golang-tcp/git/commits{/sha}",
      "comments_url": "https://api.github.com/repos/jimbirthday/golang-tcp/comments{/number}",
      "issue_comment_url": "https://api.github.com/repos/jimbirthday/golang-tcp/issues/comments{/number}",
      "contents_url": "https://api.github.com/repos/jimbirthday/golang-tcp/contents/{+path}",
      "compare_url": "https://api.github.com/repos/jimbirthday/golang-tcp/compare/{base}...{head}",
      "merges_url": "https://api.github.com/repos/jimbirthday/golang-tcp/merges",
      "archive_url": "https://api.github.com/repos/jimbirthday/golang-tcp/{archive_format}{/ref}",
      "downloads_url": "https://api.github.com/repos/jimbirthday/golang-tcp/downloads",
      "issues_url": "https://api.github.com/repos/jimbirthday/golang-tcp/issues{/number}",
      "pulls_url": "https://api.github.com/repos/jimbirthday/golang-tcp/pulls{/number}",
      "milestones_url": "https://api.github.com/repos/jimbirthday/golang-tcp/milestones{/number}",
      "notifications_url": "https://api.github.com/repos/jimbirthday/golang-tcp/notifications{?since,all,participating}",
      "labels_url": "https://api.github.com/repos/jimbirthday/golang-tcp/labels{/name}",
      "releases_url": "https://api.github.com/repos/jimbirthday/golang-tcp/releases{/id}",
      "deployments_url": "https://api.github.com/repos/jimbirthday/golang-tcp/deployments",
      "created_at": "2021-06-10T08:19:27Z",
      "updated_at": "2021-06-16T06:08:20Z",
      "pushed_at": "2021-06-16T06:46:35Z",
      "git_url": "git://github.com/jimbirthday/golang-tcp.git",
      "ssh_url": "git@github.com:jimbirthday/golang-tcp.git",
      "clone_url": "https://github.com/jimbirthday/golang-tcp.git",
      "svn_url": "https://github.com/jimbirthday/golang-tcp",
      "homepage": null,
      "size": 2933,
      "stargazers_count": 0,
      "watchers_count": 0,
      "language": "Go",
      "has_issues": true,
      "has_projects": true,
      "has_downloads": true,
      "has_wiki": true,
      "has_pages": false,
      "forks_count": 0,
      "mirror_url": null,
      "archived": false,
      "disabled": false,
      "open_issues_count": 1,
      "license": null,
      "forks": 0,
      "open_issues": 1,
      "watchers": 0,
      "default_branch": "main"
    }
  },
  "base": {
    "label": "jimbirthday:main",
    "ref": "main",
    "sha": "e094a1ed6adee25fb6ca8727fda180f5ff6659cf",
    "user": {
      "login": "jimbirthday",
      "id": 38313933,
      "node_id": "MDQ6VXNlcjM4MzEzOTMz",
      "avatar_url": "https://avatars.githubusercontent.com/u/38313933?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/jimbirthday",
      "html_url": "https://github.com/jimbirthday",
      "followers_url": "https://api.github.com/users/jimbirthday/followers",
      "following_url": "https://api.github.com/users/jimbirthday/following{/other_user}",
      "gists_url": "https://api.github.com/users/jimbirthday/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/jimbirthday/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/jimbirthday/subscriptions",
      "organizations_url": "https://api.github.com/users/jimbirthday/orgs",
      "repos_url": "https://api.github.com/users/jimbirthday/repos",
      "events_url": "https://api.github.com/users/jimbirthday/events{/privacy}",
      "received_events_url": "https://api.github.com/users/jimbirthday/received_events",
      "type": "User",
      "site_admin": false
    },
    "repo": {
      "id": 375623429,
      "node_id": "MDEwOlJlcG9zaXRvcnkzNzU2MjM0Mjk=",
      "name": "golang-tcp",
      "full_name": "jimbirthday/golang-tcp",
      "private": false,
      "owner": {
        "login": "jimbirthday",
        "id": 38313933,
        "node_id": "MDQ6VXNlcjM4MzEzOTMz",
        "avatar_url": "https://avatars.githubusercontent.com/u/38313933?v=4",
        "gravatar_id": "",
        "url": "https://api.github.com/users/jimbirthday",
        "html_url": "https://github.com/jimbirthday",
        "followers_url": "https://api.github.com/users/jimbirthday/followers",
        "following_url": "https://api.github.com/users/jimbirthday/following{/other_user}",
        "gists_url": "https://api.github.com/users/jimbirthday/gists{/gist_id}",
        "starred_url": "https://api.github.com/users/jimbirthday/starred{/owner}{/repo}",
        "subscriptions_url": "https://api.github.com/users/jimbirthday/subscriptions",
        "organizations_url": "https://api.github.com/users/jimbirthday/orgs",
        "repos_url": "https://api.github.com/users/jimbirthday/repos",
        "events_url": "https://api.github.com/users/jimbirthday/events{/privacy}",
        "received_events_url": "https://api.github.com/users/jimbirthday/received_events",
        "type": "User",
        "site_admin": false
      },
      "html_url": "https://github.com/jimbirthday/golang-tcp",
      "description": null,
      "fork": false,
      "url": "https://api.github.com/repos/jimbirthday/golang-tcp",
      "forks_url": "https://api.github.com/repos/jimbirthday/golang-tcp/forks",
      "keys_url": "https://api.github.com/repos/jimbirthday/golang-tcp/keys{/key_id}",
      "collaborators_url": "https://api.github.com/repos/jimbirthday/golang-tcp/collaborators{/collaborator}",
      "teams_url": "https://api.github.com/repos/jimbirthday/golang-tcp/teams",
      "hooks_url": "https://api.github.com/repos/jimbirthday/golang-tcp/hooks",
      "issue_events_url": "https://api.github.com/repos/jimbirthday/golang-tcp/issues/events{/number}",
      "events_url": "https://api.github.com/repos/jimbirthday/golang-tcp/events",
      "assignees_url": "https://api.github.com/repos/jimbirthday/golang-tcp/assignees{/user}",
      "branches_url": "https://api.github.com/repos/jimbirthday/golang-tcp/branches{/branch}",
      "tags_url": "https://api.github.com/repos/jimbirthday/golang-tcp/tags",
      "blobs_url": "https://api.github.com/repos/jimbirthday/golang-tcp/git/blobs{/sha}",
      "git_tags_url": "https://api.github.com/repos/jimbirthday/golang-tcp/git/tags{/sha}",
      "git_refs_url": "https://api.github.com/repos/jimbirthday/golang-tcp/git/refs{/sha}",
      "trees_url": "https://api.github.com/repos/jimbirthday/golang-tcp/git/trees{/sha}",
      "statuses_url": "https://api.github.com/repos/jimbirthday/golang-tcp/statuses/{sha}",
      "languages_url": "https://api.github.com/repos/jimbirthday/golang-tcp/languages",
      "stargazers_url": "https://api.github.com/repos/jimbirthday/golang-tcp/stargazers",
      "contributors_url": "https://api.github.com/repos/jimbirthday/golang-tcp/contributors",
      "subscribers_url": "https://api.github.com/repos/jimbirthday/golang-tcp/subscribers",
      "subscription_url": "https://api.github.com/repos/jimbirthday/golang-tcp/subscription",
      "commits_url": "https://api.github.com/repos/jimbirthday/golang-tcp/commits{/sha}",
      "git_commits_url": "https://api.github.com/repos/jimbirthday/golang-tcp/git/commits{/sha}",
      "comments_url": "https://api.github.com/repos/jimbirthday/golang-tcp/comments{/number}",
      "issue_comment_url": "https://api.github.com/repos/jimbirthday/golang-tcp/issues/comments{/number}",
      "contents_url": "https://api.github.com/repos/jimbirthday/golang-tcp/contents/{+path}",
      "compare_url": "https://api.github.com/repos/jimbirthday/golang-tcp/compare/{base}...{head}",
      "merges_url": "https://api.github.com/repos/jimbirthday/golang-tcp/merges",
      "archive_url": "https://api.github.com/repos/jimbirthday/golang-tcp/{archive_format}{/ref}",
      "downloads_url": "https://api.github.com/repos/jimbirthday/golang-tcp/downloads",
      "issues_url": "https://api.github.com/repos/jimbirthday/golang-tcp/issues{/number}",
      "pulls_url": "https://api.github.com/repos/jimbirthday/golang-tcp/pulls{/number}",
      "milestones_url": "https://api.github.com/repos/jimbirthday/golang-tcp/milestones{/number}",
      "notifications_url": "https://api.github.com/repos/jimbirthday/golang-tcp/notifications{?since,all,participating}",
      "labels_url": "https://api.github.com/repos/jimbirthday/golang-tcp/labels{/name}",
      "releases_url": "https://api.github.com/repos/jimbirthday/golang-tcp/releases{/id}",
      "deployments_url": "https://api.github.com/repos/jimbirthday/golang-tcp/deployments",
      "created_at": "2021-06-10T08:19:27Z",
      "updated_at": "2021-06-16T06:08:20Z",
      "pushed_at": "2021-06-16T06:46:35Z",
      "git_url": "git://github.com/jimbirthday/golang-tcp.git",
      "ssh_url": "git@github.com:jimbirthday/golang-tcp.git",
      "clone_url": "https://github.com/jimbirthday/golang-tcp.git",
      "svn_url": "https://github.com/jimbirthday/golang-tcp",
      "homepage": null,
      "size": 2933,
      "stargazers_count": 0,
      "watchers_count": 0,
      "language": "Go",
      "has_issues": true,
      "has_projects": true,
      "has_downloads": true,
      "has_wiki": true,
      "has_pages": false,
      "forks_count": 0,
      "mirror_url": null,
      "archived": false,
      "disabled": false,
      "open_issues_count": 1,
      "license": null,
      "forks": 0,
      "open_issues": 1,
      "watchers": 0,
      "default_branch": "main"
    }
  },
  "_links": {
    "self": {
      "href": "https://api.github.com/repos/jimbirthday/golang-tcp/pulls/1"
    },
    "html": {
      "href": "https://github.com/jimbirthday/golang-tcp/pull/1"
    },
    "issue": {
      "href": "https://api.github.com/repos/jimbirthday/golang-tcp/issues/1"
    },
    "comments": {
      "href": "https://api.github.com/repos/jimbirthday/golang-tcp/issues/1/comments"
    },
    "review_comments": {
      "href": "https://api.github.com/repos/jimbirthday/golang-tcp/pulls/1/comments"
    },
    "review_comment": {
      "href": "https://api.github.com/repos/jimbirthday/golang-tcp/pulls/comments{/number}"
    },
    "commits": {
      "href": "https://api.github.com/repos/jimbirthday/golang-tcp/pulls/1/commits"
    },
    "statuses": {
      "href": "https://api.github.com/repos/jimbirthday/golang-tcp/statuses/9a0fc20aace682c1e7dc54865e7a5eca7870d027"
    }
  },
  "author_association": "OWNER",
  "auto_merge": null,
  "active_lock_reason": null,
  "merged": false,
  "mergeable": true,
  "rebaseable": true,
  "mergeable_state": "clean",
  "merged_by": null,
  "comments": 4,
  "review_comments": 0,
  "maintainer_can_modify": false,
  "commits": 1,
  "additions": 0,
  "deletions": 100,
  "changed_files": 1
}`)

	gp := new(githubPRHook)
	err := json.Unmarshal(str, gp)
	if err != nil {
		return
	}
	if gp.Action != "" {
		if gp.Action == hook.ActionSynchronize {
			gp.Action = hook.ActionUpdate
		} else if gp.Action == hook.ActionOpened {
			gp.Action = hook.ActionOpen
		} else {
			return
		}
	}
	hook1 := convertPullRequestHook1(gp)
	fmt.Println(hook1)
}

func convertPullRequestHook1(gp *githubPRHook) *hook.PullRequestHook {
	return &hook.PullRequestHook{
		Action: gp.Action,
		Repo: hook.Repository{
			Ref:         gp.PullRequest.Head.Ref,
			Sha:         gp.PullRequest.Head.Sha,
			CloneURL:    gp.PullRequest.Head.Repo.CloneUrl,
			CreatedAt:   gp.PullRequest.Head.Repo.CreatedAt,
			Branch:      gp.PullRequest.Head.Ref,
			Description: gp.PullRequest.Head.Repo.Description,
			FullName:    gp.PullRequest.Head.Repo.FullName,
			GitHttpURL:  gp.PullRequest.Head.Repo.GitUrl,
			GitShhURL:   gp.PullRequest.Head.Repo.SshUrl,
			GitSvnURL:   gp.PullRequest.Head.Repo.SvnUrl,
			GitURL:      gp.PullRequest.Head.Repo.GitUrl,
			HtmlURL:     gp.PullRequest.Head.Repo.HtmlUrl,
			SshURL:      gp.PullRequest.Head.Repo.SshUrl,
			SvnURL:      gp.PullRequest.Head.Repo.SvnUrl,
			Name:        gp.PullRequest.Head.Repo.Name,
			Private:     gp.PullRequest.Head.Repo.Private,
			URL:         gp.PullRequest.Head.Repo.Url,
			Owner:       gp.PullRequest.Head.Repo.Owner.Login,
			RepoType:    common.GITHUB,
			RepoOpenid:  strconv.Itoa(gp.Repository.Id),
		},
		TargetRepo: hook.Repository{
			Ref:         gp.PullRequest.Base.Ref,
			Sha:         gp.PullRequest.Base.Sha,
			CloneURL:    gp.PullRequest.Base.Repo.CloneUrl,
			CreatedAt:   gp.PullRequest.Base.Repo.CreatedAt,
			Branch:      gp.PullRequest.Base.Ref,
			Description: gp.PullRequest.Base.Repo.Description,
			FullName:    gp.PullRequest.Base.Repo.FullName,
			GitHttpURL:  gp.PullRequest.Base.Repo.GitUrl,
			GitShhURL:   gp.PullRequest.Base.Repo.SshUrl,
			GitSvnURL:   gp.PullRequest.Base.Repo.SvnUrl,
			GitURL:      gp.PullRequest.Base.Repo.GitUrl,
			HtmlURL:     gp.PullRequest.Base.Repo.HtmlUrl,
			SshURL:      gp.PullRequest.Base.Repo.SshUrl,
			SvnURL:      gp.PullRequest.Base.Repo.SvnUrl,
			Name:        gp.PullRequest.Base.Repo.Name,
			Private:     gp.PullRequest.Base.Repo.Private,
			URL:         gp.PullRequest.Base.Repo.Url,
			Owner:       gp.PullRequest.Base.Repo.Owner.Login,
			RepoType:    common.GITHUB,
			RepoOpenid:  "",
		},
		PullRequest: hook.PullRequest{
			Number: gp.Number,
			Body:   gp.PullRequest.Body,
			Title:  gp.PullRequest.Title,
			Base: hook.Reference{
				Name: gp.PullRequest.Base.Ref,
				Path: gp.PullRequest.Base.Repo.Name,
				Sha:  gp.PullRequest.Base.Sha,
			},
			Head: hook.Reference{
				Name: gp.PullRequest.Head.Ref,
				Path: gp.PullRequest.Head.Repo.Name,
				Sha:  gp.PullRequest.Head.Sha,
			},
			Author: hook.User{
				UserName: gp.PullRequest.User.Login,
			},
			Created: time.Time{},
			Updated: time.Time{},
		},
		Sender: hook.User{},
	}
}

type githubPRHook struct {
	Action      string `json:"action"`
	Number      int64  `json:"number"`
	PullRequest struct {
		Url      string `json:"url"`
		Id       int    `json:"id"`
		NodeId   string `json:"node_id"`
		HtmlUrl  string `json:"html_url"`
		DiffUrl  string `json:"diff_url"`
		PatchUrl string `json:"patch_url"`
		IssueUrl string `json:"issue_url"`
		Number   int    `json:"number"`
		State    string `json:"state"`
		Locked   bool   `json:"locked"`
		Title    string `json:"title"`
		User     struct {
			Login             string `json:"login"`
			Id                int    `json:"id"`
			NodeId            string `json:"node_id"`
			AvatarUrl         string `json:"avatar_url"`
			GravatarId        string `json:"gravatar_id"`
			Url               string `json:"url"`
			HtmlUrl           string `json:"html_url"`
			FollowersUrl      string `json:"followers_url"`
			FollowingUrl      string `json:"following_url"`
			GistsUrl          string `json:"gists_url"`
			StarredUrl        string `json:"starred_url"`
			SubscriptionsUrl  string `json:"subscriptions_url"`
			OrganizationsUrl  string `json:"organizations_url"`
			ReposUrl          string `json:"repos_url"`
			EventsUrl         string `json:"events_url"`
			ReceivedEventsUrl string `json:"received_events_url"`
			Type              string `json:"type"`
			SiteAdmin         bool   `json:"site_admin"`
		} `json:"user"`
		Body               string        `json:"body"`
		CreatedAt          time.Time     `json:"created_at"`
		UpdatedAt          time.Time     `json:"updated_at"`
		ClosedAt           interface{}   `json:"closed_at"`
		MergedAt           interface{}   `json:"merged_at"`
		MergeCommitSha     interface{}   `json:"merge_commit_sha"`
		Assignee           interface{}   `json:"assignee"`
		Assignees          []interface{} `json:"assignees"`
		RequestedReviewers []interface{} `json:"requested_reviewers"`
		RequestedTeams     []interface{} `json:"requested_teams"`
		Labels             []interface{} `json:"labels"`
		Milestone          interface{}   `json:"milestone"`
		Draft              bool          `json:"draft"`
		CommitsUrl         string        `json:"commits_url"`
		ReviewCommentsUrl  string        `json:"review_comments_url"`
		ReviewCommentUrl   string        `json:"review_comment_url"`
		CommentsUrl        string        `json:"comments_url"`
		StatusesUrl        string        `json:"statuses_url"`
		Head               struct {
			Label string `json:"label"`
			Ref   string `json:"ref"`
			Sha   string `json:"sha"`
			User  struct {
				Login             string `json:"login"`
				Id                int    `json:"id"`
				NodeId            string `json:"node_id"`
				AvatarUrl         string `json:"avatar_url"`
				GravatarId        string `json:"gravatar_id"`
				Url               string `json:"url"`
				HtmlUrl           string `json:"html_url"`
				FollowersUrl      string `json:"followers_url"`
				FollowingUrl      string `json:"following_url"`
				GistsUrl          string `json:"gists_url"`
				StarredUrl        string `json:"starred_url"`
				SubscriptionsUrl  string `json:"subscriptions_url"`
				OrganizationsUrl  string `json:"organizations_url"`
				ReposUrl          string `json:"repos_url"`
				EventsUrl         string `json:"events_url"`
				ReceivedEventsUrl string `json:"received_events_url"`
				Type              string `json:"type"`
				SiteAdmin         bool   `json:"site_admin"`
			} `json:"user"`
			Repo struct {
				Id       int    `json:"id"`
				NodeId   string `json:"node_id"`
				Name     string `json:"name"`
				FullName string `json:"full_name"`
				Private  bool   `json:"private"`
				Owner    struct {
					Login             string `json:"login"`
					Id                int    `json:"id"`
					NodeId            string `json:"node_id"`
					AvatarUrl         string `json:"avatar_url"`
					GravatarId        string `json:"gravatar_id"`
					Url               string `json:"url"`
					HtmlUrl           string `json:"html_url"`
					FollowersUrl      string `json:"followers_url"`
					FollowingUrl      string `json:"following_url"`
					GistsUrl          string `json:"gists_url"`
					StarredUrl        string `json:"starred_url"`
					SubscriptionsUrl  string `json:"subscriptions_url"`
					OrganizationsUrl  string `json:"organizations_url"`
					ReposUrl          string `json:"repos_url"`
					EventsUrl         string `json:"events_url"`
					ReceivedEventsUrl string `json:"received_events_url"`
					Type              string `json:"type"`
					SiteAdmin         bool   `json:"site_admin"`
				} `json:"owner"`
				HtmlUrl             string      `json:"html_url"`
				Description         string      `json:"description"`
				Fork                bool        `json:"fork"`
				Url                 string      `json:"url"`
				ForksUrl            string      `json:"forks_url"`
				KeysUrl             string      `json:"keys_url"`
				CollaboratorsUrl    string      `json:"collaborators_url"`
				TeamsUrl            string      `json:"teams_url"`
				HooksUrl            string      `json:"hooks_url"`
				IssueEventsUrl      string      `json:"issue_events_url"`
				EventsUrl           string      `json:"events_url"`
				AssigneesUrl        string      `json:"assignees_url"`
				BranchesUrl         string      `json:"branches_url"`
				TagsUrl             string      `json:"tags_url"`
				BlobsUrl            string      `json:"blobs_url"`
				GitTagsUrl          string      `json:"git_tags_url"`
				GitRefsUrl          string      `json:"git_refs_url"`
				TreesUrl            string      `json:"trees_url"`
				StatusesUrl         string      `json:"statuses_url"`
				LanguagesUrl        string      `json:"languages_url"`
				StargazersUrl       string      `json:"stargazers_url"`
				ContributorsUrl     string      `json:"contributors_url"`
				SubscribersUrl      string      `json:"subscribers_url"`
				SubscriptionUrl     string      `json:"subscription_url"`
				CommitsUrl          string      `json:"commits_url"`
				GitCommitsUrl       string      `json:"git_commits_url"`
				CommentsUrl         string      `json:"comments_url"`
				IssueCommentUrl     string      `json:"issue_comment_url"`
				ContentsUrl         string      `json:"contents_url"`
				CompareUrl          string      `json:"compare_url"`
				MergesUrl           string      `json:"merges_url"`
				ArchiveUrl          string      `json:"archive_url"`
				DownloadsUrl        string      `json:"downloads_url"`
				IssuesUrl           string      `json:"issues_url"`
				PullsUrl            string      `json:"pulls_url"`
				MilestonesUrl       string      `json:"milestones_url"`
				NotificationsUrl    string      `json:"notifications_url"`
				LabelsUrl           string      `json:"labels_url"`
				ReleasesUrl         string      `json:"releases_url"`
				DeploymentsUrl      string      `json:"deployments_url"`
				CreatedAt           time.Time   `json:"created_at"`
				UpdatedAt           time.Time   `json:"updated_at"`
				PushedAt            time.Time   `json:"pushed_at"`
				GitUrl              string      `json:"git_url"`
				SshUrl              string      `json:"ssh_url"`
				CloneUrl            string      `json:"clone_url"`
				SvnUrl              string      `json:"svn_url"`
				Homepage            interface{} `json:"homepage"`
				Size                int         `json:"size"`
				StargazersCount     int         `json:"stargazers_count"`
				WatchersCount       int         `json:"watchers_count"`
				Language            string      `json:"language"`
				HasIssues           bool        `json:"has_issues"`
				HasProjects         bool        `json:"has_projects"`
				HasDownloads        bool        `json:"has_downloads"`
				HasWiki             bool        `json:"has_wiki"`
				HasPages            bool        `json:"has_pages"`
				ForksCount          int         `json:"forks_count"`
				MirrorUrl           interface{} `json:"mirror_url"`
				Archived            bool        `json:"archived"`
				Disabled            bool        `json:"disabled"`
				OpenIssuesCount     int         `json:"open_issues_count"`
				License             interface{} `json:"license"`
				Forks               int         `json:"forks"`
				OpenIssues          int         `json:"open_issues"`
				Watchers            int         `json:"watchers"`
				DefaultBranch       string      `json:"default_branch"`
				AllowSquashMerge    bool        `json:"allow_squash_merge"`
				AllowMergeCommit    bool        `json:"allow_merge_commit"`
				AllowRebaseMerge    bool        `json:"allow_rebase_merge"`
				DeleteBranchOnMerge bool        `json:"delete_branch_on_merge"`
			} `json:"repo"`
		} `json:"head"`
		Base struct {
			Label string `json:"label"`
			Ref   string `json:"ref"`
			Sha   string `json:"sha"`
			User  struct {
				Login             string `json:"login"`
				Id                int    `json:"id"`
				NodeId            string `json:"node_id"`
				AvatarUrl         string `json:"avatar_url"`
				GravatarId        string `json:"gravatar_id"`
				Url               string `json:"url"`
				HtmlUrl           string `json:"html_url"`
				FollowersUrl      string `json:"followers_url"`
				FollowingUrl      string `json:"following_url"`
				GistsUrl          string `json:"gists_url"`
				StarredUrl        string `json:"starred_url"`
				SubscriptionsUrl  string `json:"subscriptions_url"`
				OrganizationsUrl  string `json:"organizations_url"`
				ReposUrl          string `json:"repos_url"`
				EventsUrl         string `json:"events_url"`
				ReceivedEventsUrl string `json:"received_events_url"`
				Type              string `json:"type"`
				SiteAdmin         bool   `json:"site_admin"`
			} `json:"user"`
			Repo struct {
				Id       int    `json:"id"`
				NodeId   string `json:"node_id"`
				Name     string `json:"name"`
				FullName string `json:"full_name"`
				Private  bool   `json:"private"`
				Owner    struct {
					Login             string `json:"login"`
					Id                int    `json:"id"`
					NodeId            string `json:"node_id"`
					AvatarUrl         string `json:"avatar_url"`
					GravatarId        string `json:"gravatar_id"`
					Url               string `json:"url"`
					HtmlUrl           string `json:"html_url"`
					FollowersUrl      string `json:"followers_url"`
					FollowingUrl      string `json:"following_url"`
					GistsUrl          string `json:"gists_url"`
					StarredUrl        string `json:"starred_url"`
					SubscriptionsUrl  string `json:"subscriptions_url"`
					OrganizationsUrl  string `json:"organizations_url"`
					ReposUrl          string `json:"repos_url"`
					EventsUrl         string `json:"events_url"`
					ReceivedEventsUrl string `json:"received_events_url"`
					Type              string `json:"type"`
					SiteAdmin         bool   `json:"site_admin"`
				} `json:"owner"`
				HtmlUrl             string      `json:"html_url"`
				Description         string      `json:"description"`
				Fork                bool        `json:"fork"`
				Url                 string      `json:"url"`
				ForksUrl            string      `json:"forks_url"`
				KeysUrl             string      `json:"keys_url"`
				CollaboratorsUrl    string      `json:"collaborators_url"`
				TeamsUrl            string      `json:"teams_url"`
				HooksUrl            string      `json:"hooks_url"`
				IssueEventsUrl      string      `json:"issue_events_url"`
				EventsUrl           string      `json:"events_url"`
				AssigneesUrl        string      `json:"assignees_url"`
				BranchesUrl         string      `json:"branches_url"`
				TagsUrl             string      `json:"tags_url"`
				BlobsUrl            string      `json:"blobs_url"`
				GitTagsUrl          string      `json:"git_tags_url"`
				GitRefsUrl          string      `json:"git_refs_url"`
				TreesUrl            string      `json:"trees_url"`
				StatusesUrl         string      `json:"statuses_url"`
				LanguagesUrl        string      `json:"languages_url"`
				StargazersUrl       string      `json:"stargazers_url"`
				ContributorsUrl     string      `json:"contributors_url"`
				SubscribersUrl      string      `json:"subscribers_url"`
				SubscriptionUrl     string      `json:"subscription_url"`
				CommitsUrl          string      `json:"commits_url"`
				GitCommitsUrl       string      `json:"git_commits_url"`
				CommentsUrl         string      `json:"comments_url"`
				IssueCommentUrl     string      `json:"issue_comment_url"`
				ContentsUrl         string      `json:"contents_url"`
				CompareUrl          string      `json:"compare_url"`
				MergesUrl           string      `json:"merges_url"`
				ArchiveUrl          string      `json:"archive_url"`
				DownloadsUrl        string      `json:"downloads_url"`
				IssuesUrl           string      `json:"issues_url"`
				PullsUrl            string      `json:"pulls_url"`
				MilestonesUrl       string      `json:"milestones_url"`
				NotificationsUrl    string      `json:"notifications_url"`
				LabelsUrl           string      `json:"labels_url"`
				ReleasesUrl         string      `json:"releases_url"`
				DeploymentsUrl      string      `json:"deployments_url"`
				CreatedAt           time.Time   `json:"created_at"`
				UpdatedAt           time.Time   `json:"updated_at"`
				PushedAt            time.Time   `json:"pushed_at"`
				GitUrl              string      `json:"git_url"`
				SshUrl              string      `json:"ssh_url"`
				CloneUrl            string      `json:"clone_url"`
				SvnUrl              string      `json:"svn_url"`
				Homepage            interface{} `json:"homepage"`
				Size                int         `json:"size"`
				StargazersCount     int         `json:"stargazers_count"`
				WatchersCount       int         `json:"watchers_count"`
				Language            string      `json:"language"`
				HasIssues           bool        `json:"has_issues"`
				HasProjects         bool        `json:"has_projects"`
				HasDownloads        bool        `json:"has_downloads"`
				HasWiki             bool        `json:"has_wiki"`
				HasPages            bool        `json:"has_pages"`
				ForksCount          int         `json:"forks_count"`
				MirrorUrl           interface{} `json:"mirror_url"`
				Archived            bool        `json:"archived"`
				Disabled            bool        `json:"disabled"`
				OpenIssuesCount     int         `json:"open_issues_count"`
				License             interface{} `json:"license"`
				Forks               int         `json:"forks"`
				OpenIssues          int         `json:"open_issues"`
				Watchers            int         `json:"watchers"`
				DefaultBranch       string      `json:"default_branch"`
				AllowSquashMerge    bool        `json:"allow_squash_merge"`
				AllowMergeCommit    bool        `json:"allow_merge_commit"`
				AllowRebaseMerge    bool        `json:"allow_rebase_merge"`
				DeleteBranchOnMerge bool        `json:"delete_branch_on_merge"`
			} `json:"repo"`
		} `json:"base"`
		Links struct {
			Self struct {
				Href string `json:"href"`
			} `json:"self"`
			Html struct {
				Href string `json:"href"`
			} `json:"html"`
			Issue struct {
				Href string `json:"href"`
			} `json:"issue"`
			Comments struct {
				Href string `json:"href"`
			} `json:"comments"`
			ReviewComments struct {
				Href string `json:"href"`
			} `json:"review_comments"`
			ReviewComment struct {
				Href string `json:"href"`
			} `json:"review_comment"`
			Commits struct {
				Href string `json:"href"`
			} `json:"commits"`
			Statuses struct {
				Href string `json:"href"`
			} `json:"statuses"`
		} `json:"_links"`
		AuthorAssociation   string      `json:"author_association"`
		AutoMerge           interface{} `json:"auto_merge"`
		ActiveLockReason    interface{} `json:"active_lock_reason"`
		Merged              bool        `json:"merged"`
		Mergeable           interface{} `json:"mergeable"`
		Rebaseable          interface{} `json:"rebaseable"`
		MergeableState      string      `json:"mergeable_state"`
		MergedBy            interface{} `json:"merged_by"`
		Comments            int         `json:"comments"`
		ReviewComments      int         `json:"review_comments"`
		MaintainerCanModify bool        `json:"maintainer_can_modify"`
		Commits             int         `json:"commits"`
		Additions           int         `json:"additions"`
		Deletions           int         `json:"deletions"`
		ChangedFiles        int         `json:"changed_files"`
	} `json:"pull_request"`
	Repository struct {
		Id       int    `json:"id"`
		NodeId   string `json:"node_id"`
		Name     string `json:"name"`
		FullName string `json:"full_name"`
		Private  bool   `json:"private"`
		Owner    struct {
			Login             string `json:"login"`
			Id                int    `json:"id"`
			NodeId            string `json:"node_id"`
			AvatarUrl         string `json:"avatar_url"`
			GravatarId        string `json:"gravatar_id"`
			Url               string `json:"url"`
			HtmlUrl           string `json:"html_url"`
			FollowersUrl      string `json:"followers_url"`
			FollowingUrl      string `json:"following_url"`
			GistsUrl          string `json:"gists_url"`
			StarredUrl        string `json:"starred_url"`
			SubscriptionsUrl  string `json:"subscriptions_url"`
			OrganizationsUrl  string `json:"organizations_url"`
			ReposUrl          string `json:"repos_url"`
			EventsUrl         string `json:"events_url"`
			ReceivedEventsUrl string `json:"received_events_url"`
			Type              string `json:"type"`
			SiteAdmin         bool   `json:"site_admin"`
		} `json:"owner"`
		HtmlUrl          string      `json:"html_url"`
		Description      string      `json:"description"`
		Fork             bool        `json:"fork"`
		Url              string      `json:"url"`
		ForksUrl         string      `json:"forks_url"`
		KeysUrl          string      `json:"keys_url"`
		CollaboratorsUrl string      `json:"collaborators_url"`
		TeamsUrl         string      `json:"teams_url"`
		HooksUrl         string      `json:"hooks_url"`
		IssueEventsUrl   string      `json:"issue_events_url"`
		EventsUrl        string      `json:"events_url"`
		AssigneesUrl     string      `json:"assignees_url"`
		BranchesUrl      string      `json:"branches_url"`
		TagsUrl          string      `json:"tags_url"`
		BlobsUrl         string      `json:"blobs_url"`
		GitTagsUrl       string      `json:"git_tags_url"`
		GitRefsUrl       string      `json:"git_refs_url"`
		TreesUrl         string      `json:"trees_url"`
		StatusesUrl      string      `json:"statuses_url"`
		LanguagesUrl     string      `json:"languages_url"`
		StargazersUrl    string      `json:"stargazers_url"`
		ContributorsUrl  string      `json:"contributors_url"`
		SubscribersUrl   string      `json:"subscribers_url"`
		SubscriptionUrl  string      `json:"subscription_url"`
		CommitsUrl       string      `json:"commits_url"`
		GitCommitsUrl    string      `json:"git_commits_url"`
		CommentsUrl      string      `json:"comments_url"`
		IssueCommentUrl  string      `json:"issue_comment_url"`
		ContentsUrl      string      `json:"contents_url"`
		CompareUrl       string      `json:"compare_url"`
		MergesUrl        string      `json:"merges_url"`
		ArchiveUrl       string      `json:"archive_url"`
		DownloadsUrl     string      `json:"downloads_url"`
		IssuesUrl        string      `json:"issues_url"`
		PullsUrl         string      `json:"pulls_url"`
		MilestonesUrl    string      `json:"milestones_url"`
		NotificationsUrl string      `json:"notifications_url"`
		LabelsUrl        string      `json:"labels_url"`
		ReleasesUrl      string      `json:"releases_url"`
		DeploymentsUrl   string      `json:"deployments_url"`
		CreatedAt        time.Time   `json:"created_at"`
		UpdatedAt        time.Time   `json:"updated_at"`
		PushedAt         time.Time   `json:"pushed_at"`
		GitUrl           string      `json:"git_url"`
		SshUrl           string      `json:"ssh_url"`
		CloneUrl         string      `json:"clone_url"`
		SvnUrl           string      `json:"svn_url"`
		Homepage         interface{} `json:"homepage"`
		Size             int         `json:"size"`
		StargazersCount  int         `json:"stargazers_count"`
		WatchersCount    int         `json:"watchers_count"`
		Language         string      `json:"language"`
		HasIssues        bool        `json:"has_issues"`
		HasProjects      bool        `json:"has_projects"`
		HasDownloads     bool        `json:"has_downloads"`
		HasWiki          bool        `json:"has_wiki"`
		HasPages         bool        `json:"has_pages"`
		ForksCount       int         `json:"forks_count"`
		MirrorUrl        interface{} `json:"mirror_url"`
		Archived         bool        `json:"archived"`
		Disabled         bool        `json:"disabled"`
		OpenIssuesCount  int         `json:"open_issues_count"`
		License          interface{} `json:"license"`
		Forks            int         `json:"forks"`
		OpenIssues       int         `json:"open_issues"`
		Watchers         int         `json:"watchers"`
		DefaultBranch    string      `json:"default_branch"`
	} `json:"repository"`
	Sender struct {
		Login             string `json:"login"`
		Id                int    `json:"id"`
		NodeId            string `json:"node_id"`
		AvatarUrl         string `json:"avatar_url"`
		GravatarId        string `json:"gravatar_id"`
		Url               string `json:"url"`
		HtmlUrl           string `json:"html_url"`
		FollowersUrl      string `json:"followers_url"`
		FollowingUrl      string `json:"following_url"`
		GistsUrl          string `json:"gists_url"`
		StarredUrl        string `json:"starred_url"`
		SubscriptionsUrl  string `json:"subscriptions_url"`
		OrganizationsUrl  string `json:"organizations_url"`
		ReposUrl          string `json:"repos_url"`
		EventsUrl         string `json:"events_url"`
		ReceivedEventsUrl string `json:"received_events_url"`
		Type              string `json:"type"`
		SiteAdmin         bool   `json:"site_admin"`
	} `json:"sender"`
}
