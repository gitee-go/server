package hook

import (
	"archive/zip"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"testing"
)

func TestZip(t *testing.T) {
	srcDir := "/Users/jim/dev/tmp1/repo/600d748e421aa985c2000002/"
	zipDir := "/Users/jim/dev/tmp1/build/1/1.zip"

	err := os.MkdirAll("/Users/jim/dev/tmp1/build/1/", os.ModePerm)
	if err != nil {
		panic(err)
	}
	file, err := os.Create(zipDir)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	w := zip.NewWriter(file)
	defer w.Close()

	walker := func(path string, info os.FileInfo, err error) error {
		fmt.Printf("Crawling: %#v\n", path)
		if err != nil {
			println(err.Error())
		}
		if info.IsDir() {
			return nil
		}
		file, err := os.Open(path)
		if err != nil {
			println(err.Error())
		}
		defer file.Close()

		all := strings.ReplaceAll(path, srcDir, "")
		f, err := w.Create(all)
		if err != nil {
			println(err.Error())
		}

		_, err = io.Copy(f, file)
		if err != nil {
			println(err.Error())
		}

		return nil
	}
	err = filepath.Walk(srcDir, walker)
	if err != nil {
		panic(err)
	}
}

func TestUnzip(t *testing.T) {
	zipPath := "/Users/jim/dev/tmp1/build/600e3d02421aa9bb54000003/repo.zip"
	reader, err := zip.OpenReader(zipPath)
	if err != nil {
		println(err.Error())
	}
	defer reader.Close()

	dst := "/Users/jim/dev/tmp1/build/600e3d02421aa9bb54000003/"

	files := reader.File

	for _, file := range files {
		path := filepath.Join(dst, file.Name)
		os.MkdirAll(filepath.Dir(path), 0755)
		if file.FileInfo().IsDir() {
			continue
		}

		open, err := file.Open()
		if err != nil {
			println(err.Error())
		}

		openFile, err := os.OpenFile(path, os.O_CREATE|os.O_RDWR|os.O_TRUNC, file.Mode())
		if err != nil {
			println(err.Error())
		}

		_, err = io.Copy(openFile, open)
		if err != nil {
			println(err.Error())
		}
		open.Close()
		openFile.Close()
	}
}

func TestName(t *testing.T) {
	posix, err := regexp.CompilePOSIX("b*")
	if err != nil {
		println("err")
	}
	if posix.Match([]byte("update .giteego/giteego.yaml.")) {
		println("suc")
	} else {
		println("failed")
	}

}

func TestName12(t *testing.T) {
	decodeString, _ := hex.DecodeString("123")
	b := verifySignature([]byte("sha1=8be4f20f8bf0673864a9f47afe95944b46e35f3f"), string(decodeString), []byte(`{
  "action": "created",
  "issue": {
    "url": "https://api.github.com/repos/jimbirthday/go4start/issues/3",
    "repository_url": "https://api.github.com/repos/jimbirthday/go4start",
    "labels_url": "https://api.github.com/repos/jimbirthday/go4start/issues/3/labels{/name}",
    "comments_url": "https://api.github.com/repos/jimbirthday/go4start/issues/3/comments",
    "events_url": "https://api.github.com/repos/jimbirthday/go4start/issues/3/events",
    "html_url": "https://github.com/jimbirthday/go4start/pull/3",
    "id": 921050021,
    "node_id": "MDExOlB1bGxSZXF1ZXN0NjcwMTA5MDYw",
    "number": 3,
    "title": "1",
    "user": {
      "login": "jimbirthday",
      "id": 38313933,
      "node_id": "MDQ6VXNlcjM4MzEzOTMz",
      "avatar_url": "https://avatars.githubusercontent.com/u/38313933?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/jimbirthday",
      "html_url": "https://github.com/jimbirthday",
      "followers_url": "https://api.github.com/users/jimbirthday/followers",
      "following_url": "https://api.github.com/users/jimbirthday/following{/other_user}",
      "gists_url": "https://api.github.com/users/jimbirthday/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/jimbirthday/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/jimbirthday/subscriptions",
      "organizations_url": "https://api.github.com/users/jimbirthday/orgs",
      "repos_url": "https://api.github.com/users/jimbirthday/repos",
      "events_url": "https://api.github.com/users/jimbirthday/events{/privacy}",
      "received_events_url": "https://api.github.com/users/jimbirthday/received_events",
      "type": "User",
      "site_admin": false
    },
    "labels": [

    ],
    "state": "open",
    "locked": false,
    "assignee": null,
    "assignees": [

    ],
    "milestone": null,
    "comments": 12,
    "created_at": "2021-06-15T06:41:39Z",
    "updated_at": "2021-06-15T07:26:51Z",
    "closed_at": null,
    "author_association": "OWNER",
    "active_lock_reason": null,
    "pull_request": {
      "url": "https://api.github.com/repos/jimbirthday/go4start/pulls/3",
      "html_url": "https://github.com/jimbirthday/go4start/pull/3",
      "diff_url": "https://github.com/jimbirthday/go4start/pull/3.diff",
      "patch_url": "https://github.com/jimbirthday/go4start/pull/3.patch"
    },
    "body": "1",
    "performed_via_github_app": null
  },
  "comment": {
    "url": "https://api.github.com/repos/jimbirthday/go4start/issues/comments/861254222",
    "html_url": "https://github.com/jimbirthday/go4start/pull/3#issuecomment-861254222",
    "issue_url": "https://api.github.com/repos/jimbirthday/go4start/issues/3",
    "id": 861254222,
    "node_id": "MDEyOklzc3VlQ29tbWVudDg2MTI1NDIyMg==",
    "user": {
      "login": "jimbirthday",
      "id": 38313933,
      "node_id": "MDQ6VXNlcjM4MzEzOTMz",
      "avatar_url": "https://avatars.githubusercontent.com/u/38313933?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/jimbirthday",
      "html_url": "https://github.com/jimbirthday",
      "followers_url": "https://api.github.com/users/jimbirthday/followers",
      "following_url": "https://api.github.com/users/jimbirthday/following{/other_user}",
      "gists_url": "https://api.github.com/users/jimbirthday/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/jimbirthday/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/jimbirthday/subscriptions",
      "organizations_url": "https://api.github.com/users/jimbirthday/orgs",
      "repos_url": "https://api.github.com/users/jimbirthday/repos",
      "events_url": "https://api.github.com/users/jimbirthday/events{/privacy}",
      "received_events_url": "https://api.github.com/users/jimbirthday/received_events",
      "type": "User",
      "site_admin": false
    },
    "created_at": "2021-06-15T07:26:51Z",
    "updated_at": "2021-06-15T07:26:51Z",
    "author_association": "OWNER",
    "body": "232321",
    "performed_via_github_app": null
  },
  "repository": {
    "id": 280176626,
    "node_id": "MDEwOlJlcG9zaXRvcnkyODAxNzY2MjY=",
    "name": "go4start",
    "full_name": "jimbirthday/go4start",
    "private": false,
    "owner": {
      "login": "jimbirthday",
      "id": 38313933,
      "node_id": "MDQ6VXNlcjM4MzEzOTMz",
      "avatar_url": "https://avatars.githubusercontent.com/u/38313933?v=4",
      "gravatar_id": "",
      "url": "https://api.github.com/users/jimbirthday",
      "html_url": "https://github.com/jimbirthday",
      "followers_url": "https://api.github.com/users/jimbirthday/followers",
      "following_url": "https://api.github.com/users/jimbirthday/following{/other_user}",
      "gists_url": "https://api.github.com/users/jimbirthday/gists{/gist_id}",
      "starred_url": "https://api.github.com/users/jimbirthday/starred{/owner}{/repo}",
      "subscriptions_url": "https://api.github.com/users/jimbirthday/subscriptions",
      "organizations_url": "https://api.github.com/users/jimbirthday/orgs",
      "repos_url": "https://api.github.com/users/jimbirthday/repos",
      "events_url": "https://api.github.com/users/jimbirthday/events{/privacy}",
      "received_events_url": "https://api.github.com/users/jimbirthday/received_events",
      "type": "User",
      "site_admin": false
    },
    "html_url": "https://github.com/jimbirthday/go4start",
    "description": "学习go过程中的样例代码",
    "fork": false,
    "url": "https://api.github.com/repos/jimbirthday/go4start",
    "forks_url": "https://api.github.com/repos/jimbirthday/go4start/forks",
    "keys_url": "https://api.github.com/repos/jimbirthday/go4start/keys{/key_id}",
    "collaborators_url": "https://api.github.com/repos/jimbirthday/go4start/collaborators{/collaborator}",
    "teams_url": "https://api.github.com/repos/jimbirthday/go4start/teams",
    "hooks_url": "https://api.github.com/repos/jimbirthday/go4start/hooks",
    "issue_events_url": "https://api.github.com/repos/jimbirthday/go4start/issues/events{/number}",
    "events_url": "https://api.github.com/repos/jimbirthday/go4start/events",
    "assignees_url": "https://api.github.com/repos/jimbirthday/go4start/assignees{/user}",
    "branches_url": "https://api.github.com/repos/jimbirthday/go4start/branches{/branch}",
    "tags_url": "https://api.github.com/repos/jimbirthday/go4start/tags",
    "blobs_url": "https://api.github.com/repos/jimbirthday/go4start/git/blobs{/sha}",
    "git_tags_url": "https://api.github.com/repos/jimbirthday/go4start/git/tags{/sha}",
    "git_refs_url": "https://api.github.com/repos/jimbirthday/go4start/git/refs{/sha}",
    "trees_url": "https://api.github.com/repos/jimbirthday/go4start/git/trees{/sha}",
    "statuses_url": "https://api.github.com/repos/jimbirthday/go4start/statuses/{sha}",
    "languages_url": "https://api.github.com/repos/jimbirthday/go4start/languages",
    "stargazers_url": "https://api.github.com/repos/jimbirthday/go4start/stargazers",
    "contributors_url": "https://api.github.com/repos/jimbirthday/go4start/contributors",
    "subscribers_url": "https://api.github.com/repos/jimbirthday/go4start/subscribers",
    "subscription_url": "https://api.github.com/repos/jimbirthday/go4start/subscription",
    "commits_url": "https://api.github.com/repos/jimbirthday/go4start/commits{/sha}",
    "git_commits_url": "https://api.github.com/repos/jimbirthday/go4start/git/commits{/sha}",
    "comments_url": "https://api.github.com/repos/jimbirthday/go4start/comments{/number}",
    "issue_comment_url": "https://api.github.com/repos/jimbirthday/go4start/issues/comments{/number}",
    "contents_url": "https://api.github.com/repos/jimbirthday/go4start/contents/{+path}",
    "compare_url": "https://api.github.com/repos/jimbirthday/go4start/compare/{base}...{head}",
    "merges_url": "https://api.github.com/repos/jimbirthday/go4start/merges",
    "archive_url": "https://api.github.com/repos/jimbirthday/go4start/{archive_format}{/ref}",
    "downloads_url": "https://api.github.com/repos/jimbirthday/go4start/downloads",
    "issues_url": "https://api.github.com/repos/jimbirthday/go4start/issues{/number}",
    "pulls_url": "https://api.github.com/repos/jimbirthday/go4start/pulls{/number}",
    "milestones_url": "https://api.github.com/repos/jimbirthday/go4start/milestones{/number}",
    "notifications_url": "https://api.github.com/repos/jimbirthday/go4start/notifications{?since,all,participating}",
    "labels_url": "https://api.github.com/repos/jimbirthday/go4start/labels{/name}",
    "releases_url": "https://api.github.com/repos/jimbirthday/go4start/releases{/id}",
    "deployments_url": "https://api.github.com/repos/jimbirthday/go4start/deployments",
    "created_at": "2020-07-16T14:32:52Z",
    "updated_at": "2021-06-15T06:39:36Z",
    "pushed_at": "2021-06-15T06:41:40Z",
    "git_url": "git://github.com/jimbirthday/go4start.git",
    "ssh_url": "git@github.com:jimbirthday/go4start.git",
    "clone_url": "https://github.com/jimbirthday/go4start.git",
    "svn_url": "https://github.com/jimbirthday/go4start",
    "homepage": null,
    "size": 75,
    "stargazers_count": 0,
    "watchers_count": 0,
    "language": "Go",
    "has_issues": true,
    "has_projects": true,
    "has_downloads": true,
    "has_wiki": true,
    "has_pages": false,
    "forks_count": 0,
    "mirror_url": null,
    "archived": false,
    "disabled": false,
    "open_issues_count": 1,
    "license": null,
    "forks": 0,
    "open_issues": 1,
    "watchers": 0,
    "default_branch": "master"
  },
  "sender": {
    "login": "jimbirthday",
    "id": 38313933,
    "node_id": "MDQ6VXNlcjM4MzEzOTMz",
    "avatar_url": "https://avatars.githubusercontent.com/u/38313933?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/jimbirthday",
    "html_url": "https://github.com/jimbirthday",
    "followers_url": "https://api.github.com/users/jimbirthday/followers",
    "following_url": "https://api.github.com/users/jimbirthday/following{/other_user}",
    "gists_url": "https://api.github.com/users/jimbirthday/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/jimbirthday/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/jimbirthday/subscriptions",
    "organizations_url": "https://api.github.com/users/jimbirthday/orgs",
    "repos_url": "https://api.github.com/users/jimbirthday/repos",
    "events_url": "https://api.github.com/users/jimbirthday/events{/privacy}",
    "received_events_url": "https://api.github.com/users/jimbirthday/received_events",
    "type": "User",
    "site_admin": false
  }
}
`))
	println(b)
}

func signBody(secret, body []byte) []byte {
	computed := hmac.New(sha1.New, secret)
	computed.Write(body)
	return []byte(computed.Sum(nil))
}

func verifySignature(secret []byte, signature string, body []byte) bool {

	const signaturePrefix = "sha1="
	const signatureLength = 45 // len(SignaturePrefix) + len(hex(sha1))

	if len(signature) != signatureLength {
		return false
	}
	if !strings.HasPrefix(signature, signaturePrefix) {
		return false
	}

	actual := make([]byte, 20)
	hex.Decode(actual, []byte(signature[5:]))

	return hmac.Equal(signBody(secret, body), actual)
}
