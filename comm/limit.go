package comm

import (
	"runtime/debug"

	"gitee.com/gitee-go/core"
)

/*
Limit is Maximum number of coroutines default value
*/
var (
	//BuildLimit
	BuildLimit = 10
	//ParseLimit
	ParseLimit = 10
)

/*
InitLimit init core
*/
func initLimit() {

	defer func() {
		if err := recover(); err != nil {
			core.LogPnc.Errorf("InitWorkPath:%+v", err)
			core.LogPnc.Errorf("%s", string(debug.Stack()))
		}
	}()

	if MainCfg.ServerConf.LimitConf != nil {
		BuildLimit = MainCfg.ServerConf.LimitConf.BuildLimit
		ParseLimit = MainCfg.ServerConf.LimitConf.ParseLimit
	}
	core.Log.Infof("BuildLimit  = %d", BuildLimit)
	core.Log.Infof("ParseLimit  = %d", ParseLimit)

}
