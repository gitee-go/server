package yml

import (
	"gitee.com/gitee-go/core/models"
)

type YML interface {
	ParseYML() (*models.YML, error)
}
