package yml

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"regexp"
	"testing"
	"time"

	"gopkg.in/yaml.v2"
)

func TestReplaceVariables(t *testing.T) {
	var js = "{\n  \"version\": 1,\n  \"name\": \"go-test\",\n  \"displayName\": \"构建golang工程\",\n  \"triggers\": {\n    \"push\": {\n      \"branches\": [\n        \"master\"\n      ]\n    }\n  },\n  \"variables\": {\n    \"HELLO\": \"hello\",\n    \"WORLD\": \"world\"\n  },\n  \"stages\": [\n    {\n      \"stage\": \"maven@1\",\n      \"displayName\": \"第一个stage\",\n      \"name\": \"stage1\",\n      \"jobs\": [\n        {\n          \"job\": null,\n          \"displayName\": \"第一个job\",\n          \"name\": \"${{HELLO}}\",\n          \"environments\": {\n            \"mvn\": \"PATH=$PATH:/Users/jim/dev/apache-maven-3.6.3/bin\",\n            \"WORLD\": \"${{WORLD}}\",\n            \"HELLO\": \"${{HELLO}}\"\n          },\n          \"commands\": [\n            \"echo ${{HELLO}} && echo ${{WORLD}}\",\n            \"echo ${{mvn}}\",\n            [\n              \"echo group1  --- Test1\",\n              \"echo group1  --- Test2\",\n              \"echo group1  --- Test3\"\n            ],\n            {\n              \"group2\": [\n                \"echo group2  --- Test1\",\n                \"echo group2  --- Test2\",\n                \"echo group2  --- Test3\",\n                {\n                  \"errGroup\": \"-echo errGroup  --- Test1 -echo errGroup  --- Test2\"\n                }\n              ]\n            },\n            \"echo Hello Wolrd 3\"\n          ]\n        },\n        {\n          \"job\": null,\n          \"displayName\": \"第一个job\",\n          \"name\": \"job 2\",\n          \"commands\": [\n            \"echo Hello Wolrd job2\"\n          ],\n          \"dependsOn\": [\n            \"${{HELLO}}\"\n          ]\n        }\n      ]\n    },\n    {\n      \"stage\": \"jdk@1.8\",\n      \"displayName\": \"java环境\",\n      \"name\": \"stage2\"\n    }\n  ]\n}"
	bytes := []byte(js)
	reg := regexp.MustCompile("\\${{([^}]+)}}")
	match := reg.Match(bytes)
	if !match {
		return
	}

	all := reg.FindAllStringSubmatch(js, -1)
	if len(all) == 0 {
		return
	}

	for _, v := range all {
		println(v[0])
		println(v[1])
		println()
	}

}

type DAG struct {
	dag1 map[string]*Detail
}

/*
Detail from req
*/
type Detail struct {
	Kind        string            `yaml:"type"`
	Rule        interface{}       `yaml:"rule"`
	DisplayName string            `yaml:"displayName"`
	Description string            `yaml:"description"`
	Required    string            `yaml:"required"`
	Message     map[string]string `yaml:"message"`
}

func TestDAG(t *testing.T) {
	bt, err := ioutil.ReadFile("./veri.yaml")
	if err != nil {
		fmt.Println(err)
	}
	dag1 := map[string]*Detail{}
	err = yaml.Unmarshal(bt, dag1)
	if err != nil {
		fmt.Println(err)
	}

	js, err := json.Marshal(dag1)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(js))
}

func TestRge(t *testing.T) {
	_, err := regexp.Compile(`^.*?(!-dag)\.(yaml|yml)$`)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("suc")
	}

}

type Pipeline struct {
	Id          string              `json:"Id"  yaml:"-"`
	BuildId     string              `json:"buildId"  yaml:"-" `
	Name        string              `yaml:"name,omitempty" json:"name"`
	Version     string              `yaml:"version,omitempty" json:"version"`
	DisplayName string              `yaml:"displayName,omitempty" json:"displayName"`
	Triggers    map[string]*Trigger `yaml:"triggers,omitempty" json:"triggers"`
	Variables   map[string]string   `yaml:"variables,omitempty" json:"variables"`
	Stage       []Stage             `yaml:"stages,omitempty" json:"stages"`
}

type Trigger struct {
	AutoCancel     bool           `json:"autoCancel"`
	Timeout        string         `json:"timeout"`
	Branches       *Branch        `json:"branches"`
	Tags           *Tag           `json:"tags"`
	Paths          *Path          `json:"paths"`
	Notes          *Path          `json:"notes"`
	CommitMessages *CommitMessage `json:"commitMessages"`
}

type CommitMessage struct {
	Include []string `json:"include"`
	Exclude []string `json:"exclude"`
}

type Path struct {
	Include []string `json:"include"`
	Exclude []string `json:"exclude"`
}

type Tag struct {
	Include []string `json:"include"`
	Exclude []string `json:"exclude"`
}

type Branch struct {
	Include []string `json:"include"`
	Exclude []string `json:"exclude"`
}

type Note struct {
	Include []string `json:"include"`
	Exclude []string `json:"exclude"`
}

type Stage struct {
	Stage             string                   `yaml:"stage" json:"stage"`
	Id                string                   `json:"Id"   yaml:"-"`
	PipelineVersionId string                   `json:"pipelineVersionId"   yaml:"-"`
	BuildId           string                   `json:"buildId"   yaml:"-"`
	Name              string                   `yaml:"name,omitempty" json:"name"`
	DisplayName       string                   `yaml:"displayName,omitempty" json:"displayName"`
	Status            string                   `json:"status"   yaml:"-"`
	Error             string                   `json:"error"   yaml:"-"`
	ExitCode          int                      `json:"exit_code"   yaml:"-"`
	Started           time.Time                `json:"started"   yaml:"-"`
	Stopped           time.Time                `json:"stopped"   yaml:"-"`
	Finished          time.Time                `json:"finished"   yaml:"-"`
	Created           time.Time                `json:"created"   yaml:"-"`
	Updated           time.Time                `json:"updated"   yaml:"-"`
	Version           string                   `json:"version"   yaml:"-"`
	OnSuccess         bool                     `json:"on_success"   yaml:"-"`
	OnFailure         bool                     `json:"on_failure"   yaml:"-"`
	Labels            map[string]string        `json:"labels,omitempty" yaml:"-"`
	Steps             []map[string]interface{} `yaml:"steps,omitempty" json:"steps"`
}

func Test2(t *testing.T) {
	dag1 := &Pipeline{}
	content := "version: 1.0\nname: go-test\ndisplayName: 构建golang工程\ntriggers:                            # 流水线触发器，决定了流水线是否被触发，目前包含 Push 和 PR 两种场景，后续会考虑加入 定时器 cron 能力\n  push:                              # Push 类型配置\n    autoCancel: true                 # 自动取消选项，当同一个分支前后出现同时构建的任务，自动取消前面的任务\n    timeout: 2min                    # 【五月版本不做具体实现，默认值2H】构建超时，支持 *h*m*s *hour *min *second 的表达式。最小小于 15 秒\n    branches:                       # 分支规则\n      include:                      # 包含，支持文本，正则或统配表达式\n          - master*\n          - dev*\n      exclude:                      # 排除，支持文本，正则或统配表达式\n          - master1\n\n  pr:                                # PR 类型配置\n    autoCancel: true\n    timeout: 2min\n    # build_mode: source_branch     # 【五月版本不包含】加Value，构建分支，支持源分支，合并后的临时分支\n    branches:                       # 分支规则\n      include:                      # 包含，支持文本，正则或统配表达式\n          - master\n          - dev*\n      exclude:                      # 排除，支持文本，正则或统配表达式\n          - release*\n          - ^-[1-9]\\d*$\n    paths:                          # 【五月版本不包含】DIFF 改动文件的路径\n      include:                      # 包含，支持文本，正则或统配表达式\n          - /demo/*\n          - src/java/main.java\n      exclude:                      # 排除，支持文本，正则或统配表达式\n          - node_modules/\n    commitMessages:                \n      include:                      # 包含，支持文本，正则或统配表达式\n          - \"must build\"\n          - test\n      exclude:                      # 排除，支持文本，正则或统配表达式\n          - skipci\n          - breakbuild              # 流水线变量，用于被流水线全程引用，变量引用写法 ${{xxxx}}，支持在YAML中被定义，也支持在脚本中使用，\n    notes:                          # 【五月版本不包含】支持评论的实现\n      include:                      # 包含，支持文本，正则或统配表达式\n          - \"must build\"\n          - test\n      exclude:                      # 排除，支持文本，正则或统配表达式\n          - skipci\nvariables:\n  HELLO: ${{HELLO}}\n  ${{WORLD}}: world_yml\nstages: \n  - stage: \n    displayName: 第一个stage\n    name: stage1\n    steps:\n      - step: maven@1\n        displayName:  第二个job\n        name: job2\n        commands:\n          - echo ${{env.current_stage.displayName}}\n          - echo ${{env.current_job.displayName}}\n        jdk: 180"
	bytes := []byte(content)
	err := yaml.Unmarshal(bytes, dag1)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("123")
	println("456")
}

func Test(t *testing.T) {
	dag1 := &Pipeline{}
	content := "version: 1.0\nname: go-test\ndisplayName: 构建golang工程\ntriggers:                            # 流水线触发器，决定了流水线是否被触发，目前包含 Push 和 PR 两种场景，后续会考虑加入 定时器 cron 能力\n  push:                              # Push 类型配置\n    autoCancel: true                 # 自动取消选项，当同一个分支前后出现同时构建的任务，自动取消前面的任务\n    timeout: 2min                    # 【五月版本不做具体实现，默认值2H】构建超时，支持 *h*m*s *hour *min *second 的表达式。最小小于 15 秒\n    branches:                       # 分支规则\n      include:                      # 包含，支持文本，正则或统配表达式\n          - master*\n          - dev*\n      exclude:                      # 排除，支持文本，正则或统配表达式\n          - master1\n\n  pr:                                # PR 类型配置\n    autoCancel: true\n    timeout: 2min\n    # build_mode: source_branch     # 【五月版本不包含】加Value，构建分支，支持源分支，合并后的临时分支\n    branches:                       # 分支规则\n      include:                      # 包含，支持文本，正则或统配表达式\n          - master\n          - dev*\n      exclude:                      # 排除，支持文本，正则或统配表达式\n          - release*\n          - ^-[1-9]\\d*$\n    paths:                          # 【五月版本不包含】DIFF 改动文件的路径\n      include:                      # 包含，支持文本，正则或统配表达式\n          - /demo/*\n          - src/java/main.java\n      exclude:                      # 排除，支持文本，正则或统配表达式\n          - node_modules/\n    commitMessages:                \n      include:                      # 包含，支持文本，正则或统配表达式\n          - \"must build\"\n          - test\n      exclude:                      # 排除，支持文本，正则或统配表达式\n          - skipci\n          - breakbuild              # 流水线变量，用于被流水线全程引用，变量引用写法 ${{xxxx}}，支持在YAML中被定义，也支持在脚本中使用，\n    notes:                          # 【五月版本不包含】支持评论的实现\n      include:                      # 包含，支持文本，正则或统配表达式\n          - \"must build\"\n          - test\n      exclude:                      # 排除，支持文本，正则或统配表达式\n          - skipci\nvariables:\n  HELLO: ${{HELLO}}\n  ${{WORLD}}: world_yml\nstages: \n  - stage: \n    displayName: 第一个stage\n    name: stage1\n    steps:\n      - step: maven@1\n        displayName:  第二个job\n        name: job2\n        commands:\n          - echo ${{env.current_stage.displayName}}\n          - echo ${{env.current_job.displayName}}\n        jdk: 180"
	bytes := []byte(content)
	err := yaml.Unmarshal(bytes, dag1)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("123")
	println("456")
}

func Test1(t *testing.T) {
	content := `
     commands:
          - echo ${{HELLO}} &&
            echo ${{WORLD}}
          - echo ${{mvn}} clean
          -
            - echo group1  --- Test1
            - echo group1  --- Test2
            - echo group1  --- Test3
          - group2:
              - echo group2  --- Test1
              - echo group2  --- Test2
              - echo group2  --- Test3
          - echo Hello Wolrd 3

`
	commands := make(map[string][]interface{})
	err := yaml.Unmarshal([]byte(content), commands)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(fmt.Sprintf("%v", commands))
}

func TestName(t *testing.T) {
	if isMatch("b", "b*") {
		fmt.Println(true)
	} else {
		fmt.Println(false)
	}
}

func isMatch(s string, p string) bool {
	m, n := len(s), len(p)
	dp := make([][]bool, m+1)
	for i := 0; i <= m; i++ {
		dp[i] = make([]bool, n+1)
	}
	dp[0][0] = true
	for i := 1; i <= n; i++ {
		if p[i-1] == '*' {
			dp[0][i] = true
		} else {
			break
		}
	}
	for i := 1; i <= m; i++ {
		for j := 1; j <= n; j++ {
			if p[j-1] == '*' {
				dp[i][j] = dp[i][j-1] || dp[i-1][j]
			} else if s[i-1] == p[j-1] {
				dp[i][j] = dp[i-1][j-1]
			}
		}
	}
	return dp[m][n]
}
