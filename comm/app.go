package comm

import (
	"gitee.com/gitee-go/core/dbcore"
	"github.com/boltdb/bolt"
	"github.com/gin-gonic/gin"
)

var (
	Port      int32
	Debugs    = false
	Installed = false
	Web       *gin.Engine
	//DBMain use getDB or get a new Session
	DBMain *dbcore.DBHelper
	//BCache bolt cache db
	BCache *bolt.DB
)
