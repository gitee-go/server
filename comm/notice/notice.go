package notice

import (
	"encoding/json"
	"fmt"
	"gitee.com/gitee-go/core/common"
)

type Notice struct {
	Content  string
	Title    string
	Types    string
	Infos    string
	Url      string
	Sender   int64
	Receiver int64
}

type NoticeDetail struct {
	Version  string `json:"version"`
	Branch   string `json:"branch"`
	FullName string `json:"fullName"`
	Result   string `json:"result"`
	Msg      string `json:"msg"`

	PipelineVersionId string `json:"pipelineVersionId"`
	RepoId            string `json:"repoId"`
	Openid            string `json:"openid"`

	MStatus       string `json:"mStatus"`
	VersionStatus string ` json:"versionStatus"`
}

const (
	BUILD_SUCCESS        = "流水线:%v 构建成功"
	BUILD_FAILED         = "流水线:%v 构建失败"
	BUILD_FAILED_ERR     = "流水线:%v 构建失败 %v"
	BUILD_CANCEL         = "流水线:%v 已取消"
	YML_FAILED           = "仓库:%v yaml解析失败"
	REPO_FAILED          = "仓库:%v 拉取失败"
	BUILD_RESULT_SUCCESS = "构建成功"
	BUILD_RESULT_FAILED  = "构建失败"
)

func BuildContent(buildStatus string, arg ...interface{}) string {
	switch buildStatus {
	case common.BUILD_STATUS_ERROR:
		return fmt.Sprintf(BUILD_FAILED, arg...)
	case common.BUILD_STATUS_CANCEL:
		return fmt.Sprintf(BUILD_CANCEL, arg...)
	case common.BUILD_STATUS_OK:
		return fmt.Sprintf(BUILD_SUCCESS, arg...)
	default:
		return fmt.Sprintf("%v", arg...)
	}
}

func (c *NoticeDetail) Marshal() (string, error) {
	marshal, err := json.Marshal(c)
	if err != nil {
		return "", err
	}
	return string(marshal), nil
}
