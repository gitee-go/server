package repo

import (
	"gitee.com/gitee-go/utils/ioex"
)

func RepoToZip(repoPath string, dst string) (string, error) {
	err := ioex.Zip(repoPath, dst, true)
	if err != nil {
		return "", err
	}
	return dst, nil
}
