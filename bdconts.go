package main

import (
	"encoding/base64"
	"flag"
	"fmt"
	"gitee.com/gitee-go/utils"
	"gitee.com/gitee-go/utils/ioex"
	"io/ioutil"
	"os"
	"path/filepath"
)

/**
压缩前端目录到zip,并使用base64压缩到代码
需要使用 -d 指定前端的目录
*/
func main() {
	d := flag.String("d", "", "ui dir")
	flag.Parse()
	if d == nil || *d == "" {
		println("need -d flag")
		return
	}
	//bdsqls()
	err := bdzip(*d)
	if err != nil {
		println("bdzip err:" + err.Error())
	}
}

func bdsqls() {
	bts, _ := ioutil.ReadFile("doc/sys.sql")
	ioutil.WriteFile("comm/dbfl.go",
		[]byte(fmt.Sprintf("package comm\n\nconst sqls = `\n%s\n`", string(bts))),
		0644)
	println("sql insert go ok!!!")
}

//压缩目录,并base64编码
func bdzip(dir string) error {
	zipfl := filepath.Join(utils.HomePath(), "dist.zip")
	os.RemoveAll(zipfl)
	defer os.RemoveAll(zipfl)
	err := ioex.Zip(dir, zipfl, true)
	if err != nil {
		return err
	}
	bts, err := ioutil.ReadFile(zipfl)
	if err != nil {
		return err
	}
	cont := base64.StdEncoding.EncodeToString(bts)
	err = ioutil.WriteFile("comm/uifls.go",
		[]byte(fmt.Sprintf("package comm\n\nconst StaticPkg = \"%s\"", cont)),
		0644)
	if err != nil {
		return err
	}
	println("ui insert go ok!!!")
	return nil
}
